<?php


Route::get('/',"frontend\HomeController@index");

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout',function(){
	if (Auth::user()!=null) {
		Auth::logout();
		return redirect(url('/login'));
	}
	return redirect(url('/login'));
});
Route::get('/checkLocation/{id}','MiddlewareLocation@authenticate');
Route::group(["prefix"=>"admin","middleware"=>"auth"],function(){
		Route::get("{branch}",function($slug){

			$data = ["HN"=>"Ha noi","DN"=>"Da nang","HCM"=>"Ho Chi Minh"];
			if (isset($data[$slug])) {
				echo "Welcome to ABC ".$data[$slug]." store";
			}else{
				return abort(404);
			}
		});
		Route::get("dashboard/{branch}","backend\DashboardController@index");

		Route::get("{branch}/book","backend\BookController@index");
		Route::get("{branch}/book/delete/{id}","backend\BookController@delete");
		Route::post("{branch}/book/ajax","backend\BookController@search");

		Route::post("{branch}/book/ajax_edit","backend\BookController@search_edit");

		Route::get("{branch}/book/add","backend\BookController@view_add");
		Route::post("{branch}/book/add","backend\BookController@action_add");

		Route::post("{branch}/book/edit","backend\BookController@edit");
		Route::get("{branch}/book/delete/{id}","backend\BookController@delete");
		Route::get("{branch}/book-branch","backend\BookBranchController@index");
		Route::post("{branch}/book-branch/ajax","backend\BookBranchController@ajax");
		Route::post("{branch}/book-branch/add","backend\BookBranchController@add");

		Route::get("{branch}/author","backend\AuthorController@index");
		Route::post("{branch}/author/add","backend\AuthorController@add");
		Route::post("{branch}/author/edit","backend\AuthorController@edit");
		Route::get("{branch}/author/delete/{id}","backend\AuthorController@delete");
		Route::get("{branch}/author/search","backend\AuthorController@search");
		Route::post("{branch}/author/ajax","backend\AuthorController@booksofauthor");

		Route::get("{branch}/publisher","backend\PublisherController@index");
		Route::post("{branch}/publisher/add","backend\PublisherController@add");
		Route::post("{branch}/publisher/edit","backend\PublisherController@edit");
		Route::get("{branch}/publisher/delete/{id}","backend\PublisherController@delete");
		Route::get("{branch}/publisher/search","backend\PublisherController@search");
		Route::post("{branch}/publisher/ajax","backend\PublisherController@booksofpublisher");

		Route::get("{branch}/genre/","backend\GenreController@index");
		Route::post("{branch}/genre/add","backend\GenreController@add");
		Route::post("{branch}/genre/edit","backend\GenreController@edit");
		Route::get("{branch}/genre/delete/{id}","backend\GenreController@delete");
		Route::get("{branch}/genre/search","backend\GenreController@search");
		Route::get("{branch}/genre/search/ajax","backend\GenreController@booksofgenre");

		Route::get("{branch}/discount/","backend\DiscountController@index");
		Route::post("{branch}/discount/add","backend\DiscountController@add");
		Route::post("{branch}/discount/edit","backend\DiscountController@edit");
		Route::get("{branch}/discount/delete/{id}","backend\DiscountController@delete");

		Route::get("{branch}/discount/search","backend\DiscountController@search");
		Route::get("{branch}/discount/mixed","backend\DiscountController@mixed");
		Route::post("{branch}/discount/ajax/","backend\DiscountController@discountof");

		Route::get("{branch}/customer","backend\CustomerController@index");
		Route::get("{branch}/customer/orders/{id}","backend\CustomerController@show_order");


		Route::post("{branch}/discount/mixed-book","backend\DiscountController@mixed_book");
		Route::post("{branch}/discount/mixed-customer","backend\DiscountController@mixed_customer");
		Route::post("{branch}/discount/statistics","backend\DiscountController@statistics");
		Route::get("{branch}/discount/mixed/delete/{mixed}/{id}/{discount}","backend\DiscountController@mixed_delete");
		Route::get("{branch}/orders/active/{id}","backend\DashboardController@active_order");
		Route::get("{branch}/orders/delete/{id}","backend\DashboardController@delete_order");

		Route::get("{branch}/orders/success/{id}","backend\DashboardController@success_order");
		Route::get("{branch}/profile","backend\DashboardController@profile");
		Route::post("{branch}/profile/save","backend\DashboardController@profile_save");


		Route::get("{branch}/server","backend\InfoController@server_info");

		Route::get("{branch}/check","backend\DashboardController@check");
		Route::post("{branch}/server/save","backend\InfoController@server_save");

		Route::post("{branch}/user/list","backend\DashboardController@list_user");

		

});
Route::post("/cart/add","frontend\HomeController@cart_add");
Route::post("/api/genres","frontend\HomeController@get_genres");
Route::get("customer/register","frontend\CustomerController@register");
Route::post("customer/register","frontend\CustomerController@do_register");
Route::get("/customer/login","frontend\CustomerController@login");
Route::post("/customer/login","frontend\CustomerController@do_login");
Route::get("/customer/logout","frontend\CustomerController@do_logout");
Route::get("/customer/profile","frontend\CustomerController@profile");
Route::get("/customer/profile/save","frontend\CustomerController@profile_save");

Route::get("/search/","frontend\HomeController@search");
Route::post("/search/details","frontend\HomeController@do_search");

Route::get("/view-cart","frontend\HomeController@view_cart");
Route::get("/view-cart/delete/{id}","frontend\HomeController@remove_cart");

Route::get("/check-out","frontend\HomeController@check_out");
Route::post("/check-out/save","frontend\HomeController@check_out_save");

Route::get("/books/detail/{id}","frontend\HomeController@detail");
Route::post("/cart/detail/add","frontend\HomeController@cart_add_detail");
Route::post("/api/server","frontend\HomeController@server_details");