<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table = "orders_details";

    protected $fillable = [
    	"book_id","order_id","quantity","branch_id","amount"
    ];
    public function book(){
    	return $this->belongsTo("App\Book");
    }
    public function order(){
    	return $this->belongsTo("App\Order");
    }
    public function branch(){
    	return $this->belongsTo("App\Branch");
    }
}