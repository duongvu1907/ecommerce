<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
   protected $table = "discounts";

    protected $fillable = [
    	"name","value","expiry_at","created_at","updated_at"
    ];
    public function books(){
    	return $this->belongsToMany("App\Book","discounts_books","discount_id","book_id");
    }
    public function customers(){
    	return $this->belongsToMany("App\Customer","discounts_customers","discount_id","customer_id");
    }
    // public $timestamp = false;
}
