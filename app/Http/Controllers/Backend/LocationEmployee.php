<?php  
 namespace App\Http\Controllers\Backend;
	use DB;
	use Auth;
	use File;
	use Carbon\Carbon;
	trait LocationEmployee
	{
		protected static $server;
		
		public function loadServer(){
			 self::$server = config('server');
		}

	    public function ConnectionDB($slug)
	    {
	        $id = self::$server[$slug]==1?"":self::$server[$slug];
	        return DB::connection('sqlsrv'.$id);
	        // return "ok";
	    }
	    public function ConnectionObject($model,$slug){
	    	$str = "App\\".$model;
	    	$obj = new $str;
	    	$sqlsrv = "sqlsrv";
	    	if (self::$server[$slug]!=1) {
	    		$sqlsrv = $sqlsrv.self::$server[$slug]."";
	    		
	    	}
	    	$obj->connection = $sqlsrv;
	    	// echo $obj->connection;
	    	$obj->setConnection($sqlsrv);
	    	// $obj = DB::connection('sqlsrv'.self::$server[$slug])->table(strtolower($model)."s");
	    	return $obj;
	    }
	    public function CheckAuth($slug){
	    	$id = Auth::user()->branch_id;
	    	$branch_id =0;
	    	$sqlsrv = "sqlsrv";
	    	if (array_key_exists($slug,self::$server)) {
	    		$branch_id = self::$server[$slug];
	    	}	    	
	    	Auth::user()->connection = $branch_id==1?$sqlsrv:$sqlsrv.$branch_id;

	    	if ($id==$branch_id) {
	    		return true;
	    	}
	    	return false;
	    }
	    public function delete_file($cd){
	    	if (File::exists($cd)) {
	    		File::delete($cd);
	    	}
	    }
	    public function CheckFile($dir,$file){
	    	if (File::exists('upload/'.$dir.$file)) {
	    		$dotname = explode(".",$file)[count(explode(".",$file))-1];
	    		return str_replace(".","",str_replace($dotname,"",$file)).str_random(5).".".$dotname;
	    	}
	    	return $file;
	    }
	    public function processDateTime($request,$current=null){
	    	date_default_timezone_set('Asia/Ho_Chi_Minh');
	    	$time_at = "00:00:00";
	    	if (is_null($current)) {
	    		$time_at = date('h:i:s ', time());
	    	}
	    	$time = $request." ".$time_at;
	    	$format = 'Y-m-d h:i:s';
            $time = date($format, strtotime($time));
            return Carbon::createFromFormat('Y-m-d h:i:s',$time);
	    }
	    public function CheckRoles($slug,$check){
	    	$id = Auth::user()->branch_id;
	    	
	    }
	    // public function RehibilitateServer($sqlsrv){
	    // 	if (config("database.connections.".$sqlsrv.".status")=="maintenance") {
	    		
	    // 	}
	    // 	return abort(404);
	    // }
	    public function check_book_id($server,$old_id){
            $new_id = $old_id+1;
            $check = DB::connection($server)->select("
                DECLARE @value INT 
                EXEC @value = check_id_book @book_id = ".$new_id."
                SELECT @value
            ");
            if ($check==1) {
                $this->check_id($server,$new_id);
            }else{
                return $new_id;
            }
        }
	}