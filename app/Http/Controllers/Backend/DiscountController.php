<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Requests\DisRequest;
use App\Http\Requests\EditDisRequest;
use App\Http\Controllers\Controller;
use Auth;
use App\Discount;
use Carbon\Carbon;
class DiscountController extends Controller
{
    use LocationEmployee;
    function __construct(){
    	$this->loadServer();
    }
    public function index($slug){
    	if ($this->CheckAuth($slug)) {
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $date = date('m-d-Y h:i:s ', time());
            $discounts_expiried = $this->ConnectionObject("Discount",$slug)->where("expiry_at","<",$date)->paginate(15);
            $discounts_available = $this->ConnectionObject("Discount",$slug)->where("expiry_at",">=",$date)->paginate(15);
    		return view("admin.discount",[
                "slug"=>$slug,
                "discounts_available"=>$discounts_available,
                "discounts_expiried"=>$discounts_expiried
            ]);
    	}
    	return abort(404);
    }
    public function add(DisRequest $request,$slug){
        if ($this->CheckAuth($slug)) {
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $validated = $request->validated();
            
            $expiry_at = $this->processDateTime($request->expiry_at,$request->time_expiry_at);
            $created_at = isset($request->created_at)?$this->processDateTime($request->created_at,$request->time_created_at):$this->processDateTime(date('Y:m:d ', time()),$request->time_created_at);

            $discount = $this->ConnectionObject('Discount',$slug)->create([
                "name"=>$request->name,
                "value"=>$request->value,
                "expiry_at"=>Carbon::createFromFormat('Y-m-d h:i:s',$expiry_at),
                "created_at"=>Carbon::createFromFormat('Y-m-d h:i:s',$created_at),
                "updated_at"=>Carbon::createFromFormat('Y-m-d h:i:s',$created_at)
            ]);
            return redirect(url('admin/'.$slug.'/discount'));
            
        } return abort(404);
    }
    public function edit(EditDisRequest $request,$slug){
        if ($this->CheckAuth($slug)) {
            $validated = $request->validated();
            $id = $request->id;
            $discount = $this->ConnectionObject('Discount',$slug)->where("id",$id)->first();

            if (isset($request->expiry_at)) {
                $expiry_at = $this->processDateTime($request->expiry_at,$request->time_expiry_at);
                $discount->expiry_at = $expiry_at;
            }
            if (isset($request->created_at)) {
                $created_at = isset($request->created_at)?$this->processDateTime($request->created_at,$request->time_created_at):$this->processDateTime(date('Y:m:d ', time()),$request->time_created_at);
                $discount->created_at = $created_at;   
            }

            $discount->name = $request->name;
            $discount->value = $request->value;
            $discount->updated_at = $this->processDateTime(date('Y:m:d ', time()));
            $discount->save();
            return redirect(url('/admin/'.$slug.'/discount'));

        }
        return abort(404);
    }
    public function delete($slug,$id){
        if ($this->CheckAuth($slug)) {
            $discount = $this->ConnectionObject('Discount',$slug)->where("id",$id)->first();
            $discount->books()->detach();
            $discount->customers()->detach();
            $discount->delete();
            return redirect(url('/admin/'.$slug.'/discount'));
        }
    }
    public function mixed($slug){
        if ($this->CheckAuth($slug)) {
            $customers = $this->ConnectionObject("Customer",$slug)->get();
            $discounts = $this->ConnectionObject("Discount",$slug)->get();
            $books = $this->ConnectionObject("Book",$slug)->get();
            return view('admin.discount_mixed',[
                'slug'=>$slug,
                "customers"=>$customers,
                "discounts"=>$discounts,
                "books"=>$books
            ]);
        }
        return abort(404);
    }
    public function mixed_book(Request $request,$slug){
        if ($this->CheckAuth($slug)) {
            $discount_id = $request->discount_id;
            $books_id = $request->book_id;

            $discount = $this->ConnectionObject("Discount",$slug)->where("id",$discount_id)->first();
            // var_dump($discount);
            $discount->books()->attach($books_id);
            return redirect(url('/admin/'.$slug.'/discount/mixed'));
        }
        return abort(404);
    }
    public function mixed_customer(Request $request,$slug){
        if ($this->CheckAuth($slug)) {
            $discount_id = $request->discount_id;
            $customer_id = $request->customer_id;
            $discount = $this->ConnectionObject("Discount",$slug)->where("id",$discount_id)->first();
            $discount->customers()->attach($customer_id);
            return redirect(url('/admin/'.$slug.'/discount/mixed'));
        }
        return abort(404);
    }
    public function statistics(Request $request,$slug){
        if ($this->CheckAuth($slug)) {
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $discount_id = $request->discount_id;
            $choose = $request->choose;
            $discount = $this->ConnectionObject("Discount",$slug)->where("id",$discount_id)->first();
            $data = [];
            if ($choose=="book") {
                $books = $discount->books()->get();
                foreach ($books as $book) {
                   array_push($data,[
                    "discount_id"=>$discount->id,
                    "id"=>$book->id,
                    "discount"=>$discount->name,
                    "name"=>$book->name,
                    "status"=>$discount->expiry_at >= date('Y-m-d h:i:s ', time())?1:0,
                    "mixed"=>"book"
                ]);
                }
                return json_encode($data);
            }
            if ($choose=="customer") {
                $customers = $discount->customers()->get();
                foreach ($customers as $customer) {
                    array_push($data,  [
                    "discount_id"=>$discount->id,
                    "id"=>$customer->id,
                    "discount"=>$discount->name,
                    "name"=>$customer->name,
                    "status"=>$discount->expiry_at >= date('Y-m-d h:i:s ', time())?1:0,
                    "mixed"=>"customer"
                ]);
                }
                return json_encode($data);
            }
            return null;
        }
        return null;
    }
    public function mixed_delete($slug,$mixed,$id,$discount_id){
        if ($this->CheckAuth($slug)) {
            $discount = $this->ConnectionObject("Discount",$slug)->where("id",$discount_id)->first();
            if ($mixed=="customer") {
                $discount->customers()->wherePivot("customer_id",$id)->detach();            
            }        
            if ($mixed=="book") {
                $discount->books()->wherePivot("book_id",$id)->detach();
            }
            return redirect(url('/admin/'.$slug.'/discount/mixed'));
        }
        return abort(404);
    }

}
