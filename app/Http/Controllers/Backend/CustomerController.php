<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Customer;
class CustomerController extends Controller
{
    use LocationEmployee;
    function __construct(){
    	$this->loadServer();
    }
    public function index($slug){
    	
    	if ($this->CheckAuth($slug)) {
    		$customer = $this->ConnectionObject("Customer",$slug)->orderBy("id","DESC")->get();
    		return view("admin.customer",[
                "slug"=>$slug,
    			"customer"=>$customer
    		]);
    	}
    }
    public function show_order($slug,$id){
        if ($this->CheckAuth($slug)) {
            $orders = $this->ConnectionObject("Customer",$slug)->where('id',$id)->first()->orders;
            return view("admin.customer_order",[
                'slug'=>$slug,
                'orders'=>$orders
            ]);
        }
        return abort(404); 
    }

}
