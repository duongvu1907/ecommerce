<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Requests\AuthorRequest;
use App\Http\Controllers\Controller;
use App\Author;
class AuthorController extends Controller
{
    use LocationEmployee;
    function __construct(){
    	$this->loadServer();
    }
    public function index($slug){
    	if ($this->CheckAuth($slug)) {
    		$authors = $this->ConnectionObject("Author",$slug)->paginate(15);
    		return view("admin.author",[
    			'slug'=>$slug,
    			'authors'=>$authors
    		]);
    	}
    		return abort(404);
    }
    public function add(AuthorRequest $request,$slug){

    	if ($this->CheckAuth($slug)) {
    	    $validated = $request->validated();
    	    $author = $this->ConnectionObject("Author",$slug);
    	    $author->create($request->all());
    		return redirect(url("admin/".$slug."/author"));
    	}
    	return abort(404);
    }
    public function edit(AuthorRequest $request,$slug){
    	if ($this->CheckAuth($slug)) {
    		$validated = $request->validated();
    		$id = (int)$request->id;
    		$author = $this->ConnectionObject("Author",$slug);
    		$author->findOrFail($id)->fill($request->all())->save();
    		return redirect(url("admin/".$slug."/author"));                                                                   
    	} return abort(404);
    }
    public function delete($slug,$id){
    	if ($this->CheckAuth($slug)) {
    		$author = $this->ConnectionObject("Author",$slug);
    		if ($author->findOrFail($id)!=null) {
    			$author->find($id)->delete();
    		}
    		return redirect(url("/admin/".$slug."/author"));
    	}	
    	return abort(404);
    }
    public function search($slug){
    	if ($this->CheckAuth($slug)) {
    		$authors = $this->ConnectionObject("Author",$slug)->get();
    		return view('admin.edit.author_search',[
    			'slug'=>$slug,
    			'authors'=>$authors
    		]);
    	}return abort(404);
    }
    public function booksofauthor(Request $request,$slug){
    	if ($this->CheckAuth($slug)) {

    		$author = $this->ConnectionObject("Author",$slug)->where('id',$request->author_id)->first();
    		$books  = $author->books()->get();
            $data = [];
            foreach ($books as $book) {
                $bb = $book->branches_books()->get();
                $available = 0;
                foreach ($bb as $b) {
                    $available+=$b->quantity;
                }
                array_push($data,[
                    "id"=>$book->id,
                    "name"=>$book->name,
                    "available"=>$available
                ]);
            }
    		return json_encode($data);
    	}
    	return null;
    }
}
