<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Requests\BookBranchRequest;
use App\Http\Controllers\Controller;
use App\Branch_Book;
use App\Branch;
class BookBranchController extends Controller
{
    use LocationEmployee;
    function __construct(){
    	$this->loadServer();
    }
    public function index($slug){
    	if ($this->CheckAuth($slug)) {
    		$books_branches = $this->ConnectionObject("Branch_Book",$slug)->orderBy('id','desc')->paginate(5);
    		$branches = $this->ConnectionObject("Branch",$slug)->get();
    		// var_dump($branches);
    		return view('admin.branch_book',[
    			'slug'=>$slug,
    			'books_branches'=>$books_branches,
    			'branches'=>$branches
    		]);
    	}
    	return abort(404);
    }
    public function ajax(Request $request,$slug){
    	if ($this->CheckAuth($slug)) {
    		$name = "%".$request->name."%";
    		$bb = $this->ConnectionObject("Book",$slug)->where("name","LIKE",$name)->get();
    		return json_encode($bb);
    	}
    	return null;
    }
    public function add(BookBranchRequest $request,$slug){
    	if ($this->CheckAuth($slug)) {
    		$validated = $request->validated();
    		$book_id = (int)$request->book_id;
    		$branch_id = (int)$request->branch_id;
    		$quantity = (int)$request->quantity;
    		$sample = $this->ConnectionObject("Branch_Book",$slug)->where("book_id",$book_id)->where("branch_id",$branch_id)->first();
    		if (is_null($sample)) {
    			$this->ConnectionObject("Branch_Book",$slug)->create([
    			"book_id"=>$book_id,
    			"branch_id"=>$branch_id,
    			"quantity"=>$quantity
    			]);
    		}else{
    			$sample->quantity = (int)$sample->quantity+$quantity;
    			 $sample->save();
    		}

    		return redirect(url("admin/".$slug."/book-branch"));
    	}
    	return abort(404);
    }
    
}
