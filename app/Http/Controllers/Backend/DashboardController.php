<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \DB;
use Auth;
use User;
use App\Customer;
class DashboardController extends Controller
{
    use LocationEmployee;
    function __construct(){
    	$this->loadServer();
    }

    public function index($slug){
    	
		// $this->CheckAuth($slug);
    	if ($this->CheckAuth($slug)) {

            
            $book_sell_id =$this->ConnectionDB($slug)->select("
                        SELECT TOP 10 orders_details.book_id ,sum(quantity) as qty
                        From orders_details
                        group by book_id
                        Order by qty DESC
                ");
            $customer_sell_id =$this->ConnectionDB($slug)->select("
                        SELECT TOP 10 orders_details.order_id ,sum(quantity) as qty
                        From orders_details
                        group by order_id
                        Order by qty DESC
                ");
            $branch_sell_id = $this->ConnectionDB($slug)->select("
                        SELECT TOP 10 orders_details.branch_id ,sum(quantity) as qty
                        From orders_details
                        group by branch_id
                        Order By qty DESC
                ");
            // $book = $this->ConnectionObject($this->server,"Book")::find($book_id)->get();
            // var_dump($book_id);
            $data = array();
            $data_customer = array();
            $data_branch = array();
            foreach ($customer_sell_id as $c) {
                $order = $this->ConnectionObject("Order",$slug)->where("id",$c->order_id)->first();
                $cs = $order->customer;
                $cs->quantity = $c->qty;
                array_push($data_customer,$cs);
            }
            foreach ($book_sell_id as $id) {

                $b = $this->ConnectionObject("Book",$slug)->where("id",$id->book_id)->first();
                $b->quantity = $id->qty;
                // var_dump($id);
                array_push($data,$b);
            }
            foreach ($branch_sell_id as $id) {
                // var_dump($id);
                $br = $this->ConnectionObject("Branch",$slug)->where("id",$id->branch_id)->first() ;
                if ($br) {
                    $br->quantity = $id->qty;
                }
                array_push($data_branch,$br);
            }
            $orders = $this->ConnectionObject("Order",$slug)->orderBy("id","DESC")->take(5)->get();
    		return view('admin.dashboard',[
    			"slug"=>$slug,
                "books"=>$data,
                "orders"=>$orders,
                "customers"=>$data_customer,
                "branches"=>$data_branch
    		]);
    	}else{
    		return abort(404);
    	}
   
    }
    public function active_order($slug,$id){
        if ($this->CheckAuth($slug)) {
            $order = $this->ConnectionObject("Order",$slug)->where("id",$id)->first();
            $orders_details = $this->ConnectionObject("OrderDetail",$slug)->where("order_id",$id)->get();
            $customer =  $order->customer;
            return view("admin.edit.order_active",[
                'slug'=>$slug,
                'order'=>$order,
                'orders_details'=>$orders_details,
                'customer'=>$customer
            ]);
        }
        return abort(404);
    }
    public function success_order($slug,$id){
        if ($this->CheckAuth($slug)) {
            $order = $this->ConnectionObject("Order",$slug)->where("id",$id)->first();
            $order->status=true;
            $order->save();
            return redirect(url('/admin/dashboard/'.$slug));
        }
        return abort(404);
    }
    public function profile($slug){
        if ($this->CheckAuth($slug)) {
            $branch = $this->ConnectionObject('Branch',$slug)->get();
            return  view('admin.edit.profile_edit',[
                "slug"=>$slug,
                "branches"=>$branch,
                "user"=>Auth::user()
            ]);
        }
        return abort(404);
    }
    public function profile_save(Request $request,$slug){
        if ($this->CheckAuth($slug)) {
            $id = Auth::user()->id;
            $user = $this->ConnectionObject('User',$slug)->where("id",$id)->first();
            $user->name = $request->name;
            if (isset($request->password)) {
                 $user->password = Hash::make($request->password);
                 $user->save();
                 Auth::logout();
                 return redirect(url('/login'));
            }
            $user->save();
            return redirect(url('/admin/dashboard/'.$slug));
        }
        return abort(404);
    }
    public function check($slug){
         if ($this->CheckAuth($slug)) {
            $book_sell_id = $this->ConnectionDB($slug)->select("
                        SELECT TOP 10 dn.book_id ,sum(dn.quantity+hcm.quantity) as qty
                        From orders_details dn inner join LINK.Store.dbo.orders_details hcm
                        
                      ");
            var_dump($book_sell_id);
         }
         return abort(404); 
    }
    public function list_user($slug){
        if ($this->CheckAuth($slug)) {
            $users = $this->ConnectionObject('Users',$slug)->get();
            return view("admin.list_user",[
                "users"=>$users
            ]);
        }
        return abort(404);
    }
}
