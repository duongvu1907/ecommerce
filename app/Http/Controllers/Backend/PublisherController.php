<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PublisherRequest;
use App\Publisher;
use App\Book;
use Auth;
class PublisherController extends Controller
{
     use LocationEmployee;
    function __construct(){
    	$this->loadServer();
    }
    public function index($slug){
    	if ($this->CheckAuth($slug)) {
    		$publishers = $this->ConnectionObject("Publisher",$slug)->paginate(15);
    		return view("admin.publisher",[
    			'slug'=>$slug,
    			'publishers'=>$publishers
    		]);
    	}
    		return abort(404);
    }
    public function add(PublisherRequest $request,$slug){
    	if ($this->CheckAuth($slug)) {
    		$validated = $request->validated();
    		 $this->ConnectionObject("Publisher",$slug)->create($request->all());
    		 return redirect(url('/admin/'.$slug.'/publisher'));
    	}
    	return abort(404);
    }
    public function edit(PublisherRequest $request,$slug){
    	
    	if ($this->CheckAuth($slug)){
    		$validated = $request->validated();
    		$id = (int)$request->id;
    		$this->ConnectionObject("Publisher",$slug)->where('id',$id)->fill($request->all())->save();
    		 return redirect(url('/admin/'.$slug.'/publisher'));
    	}
    	return abort(404);
    }
    public function search($slug){
    	if ($this->CheckAuth($slug)) {
    		$publishers = $this->ConnectionObject("Publisher",$slug)->get();
    		return view('admin.edit.publisher_search',[
    			"slug"=>$slug,
    			"publishers"=>$publishers
    		]);
    	}	
    	return abort(404);
    }
    public function booksofpublisher(Request $request,$slug){
    	if ($this->CheckAuth($slug)) {
    		$publisher_id = $request->publisher_id;
            $books = $this->ConnectionObject("Publisher",$slug)->where('id',$publisher_id)->first()->books()->get();
            $data = [];
            foreach ($books as $book) {
                $bb = $book->branches_books()->get();
                $available = 0;
                foreach ($bb as $b) {
                    $available+=$b->quantity;
                }
                array_push($data,[
                    "id"=>$book->id,
                    "name"=>$book->name,
                    "available"=>$available
                ]);
            }

    		return json_encode($data);
    	}
    	return null;
    }
    public function delete($slug,$id){
    	if ($this->CheckAuth($slug)) {
    		$this->ConnectionObject("Publisher",$slug)->where('id',$id)->delete();
    		return redirect(url("admin/".$slug."/publisher"));
    	}
    		return abort(404);
    }
}
