<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Requests\BookRequest;
use App\Http\Controllers\Controller;
use App\Author;
use App\Publisher;
use App\Genre;
use File;
use Auth;
class BookController extends Controller
{
    use LocationEmployee;
    function __construct(){
    	$this->loadServer();
    }
    public function index($slug){
    	if ($this->CheckAuth($slug)) {
    		$books= $this->ConnectionObject("Book",$slug)->orderBy('id','desc')->paginate(15);
    		$authors = $this->ConnectionObject("Author",$slug)->get();
    		$publishers = $this->ConnectionObject("Publisher",$slug)->get();
    		$genres = $this->ConnectionObject("Genre",$slug)->get();
    		return view("admin.book",[
    			'slug'=>$slug,
    			'books'=>$books,
    			"authors"=>$authors,
    			"publishers"=>$publishers,
    			"genres"=>$genres
    		]);
    	}else{
    		return abort(404);
    	}
    	
    }

    public function view_add($slug){
    	if ($this->CheckAuth($slug)) {
    		$authors = $this->ConnectionObject("Author",$slug)->get();
    		$publishers = $this->ConnectionObject("Publisher",$slug)->get();
    		$genres = $this->ConnectionObject("Genre",$slug)->get();
    		return view('admin.edit.add_book',[
    			"slug"=>$slug,
    			"authors"=>$authors,
    			"publishers"=>$publishers,
    			"genres"=>$genres
    		]);
    	}
    	return abort(404);
    }
    public function edit(BookRequest $request,$slug){
    	if ($this->CheckAuth($slug)) {
    		$validated = $request->validated();
    		$id = $request->id;
    		$book = $this->ConnectionObject("Book",$slug)->where("id",$id)->first();
    		$file = $request->file("image");
    		if ($file!=null) {
    			$file_name = $this->CheckFile("books/",$file->getClientOriginalName());
	    		File::move($file,"upload/books/".$file_name);
	    		$this->delete_file('upload/books/'.$book->images);
	    		$book->images = $file_name;
    		}

    		$book->name = $request->name;
    		$book->public = $request->public;
    		$book->price = $request->price;
    		$book->edition = $request->edition;
    		$book->author_id = $request->author_id;
    		$book->publisher_id = $request->publisher_id;
    		$book->save();
    		if ($book->save()) {
    			$book->genres()->detach();
    			$book->genres()->attach($request->genre);
    		}
    		return redirect(url('admin/'.$slug.'/book/'));
    	} return abort(404);
    }
    public function action_add(BookRequest $request,$slug){
    		if ($this->CheckAuth($slug)) {
    			$validated = $request->validated();
    			$file = $request->file("image");
    			$file_name = $this->CheckFile("books/",$file->getClientOriginalName());
    			File::move($file,"upload/books/".$file_name);
                $id_old = $this->ConnectionObject("Book",$slug)->orderBy("id","DESC")->first()->id;
                $id_new = $this->check_book_id(Auth::user()->connection,$id_old);

    			$this->ConnectionObject("Book",$slug)->create([
                    "id"=>$id_new,
    				"name"=>$request->name,
    				"public"=>$request->public,
    				"price"=>$request->price,
    				"edition"=>$request->edition,
    				"author_id"=>$request->author_id,
    				"publisher_id"=>$request->publisher_id,
    				"images"=>$file_name
    			]);
                $book = $this->ConnectionObject("Book",$slug)->orderBy("id","DESC")->first();
    			$book->genres()->attach($request->genre);
    			return redirect(url('admin/'.$slug.'/book/'));

    		}
    		return abort(404);
    }
    public function search_edit(Request $request,$slug){
    	if ($this->CheckAuth($slug)) {
    		$book = $this->ConnectionObject("Book",$slug)->where("id",$request->id)->first();
    		$genres = $book->genres()->get();
    		$data = [
    			"book"=>$book,
    			"genres"=>$genres
    		];
    		return json_encode($data);
    	}
    	return null;
    }
    public function delete($slug,$id){
        // echo 'string';
        if ($this->CheckAuth($slug)) {
            $this->ConnectionObject("Book",$slug)->where("id","=",$id)->delete();
            return redirect(url('admin/'.$slug.'/book/'));
        }
        return abort(404);
    }
}
