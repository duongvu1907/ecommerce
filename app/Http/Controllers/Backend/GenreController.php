<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\GenreRequest;
use App\Genre;
class GenreController extends Controller
{
    use LocationEmployee;
    function __construct(){
    	$this->loadServer();
    }
    public function index($slug){
    	if ($this->CheckAuth($slug)) {
    		$genres = $this->ConnectionObject("Genre",$slug)->paginate(15);
    		return view("admin.genre",[
    			'slug'=>$slug,
    			'genres'=>$genres
    		]);
    	}
    	return abort(404);
    }
    public function add(GenreRequest $request,$slug){
    	if ($this->CheckAuth($slug)) {
    	    $validated = $request->validated();
    	    $genre = $this->ConnectionObject("Genre",$slug);
    	    $genre->create($request->all());
    		return redirect(url("admin/".$slug."/genre"));
    	}
    	return abort(404);
    }
     public function edit(GenreRequest $request,$slug){
    	if ($this->CheckAuth($slug)) {
    		$validated = $request->validated();
    		$id = (int)$request->id;
    		$genre = $this->ConnectionObject("Genre",$slug)->where("id",$id)->first();
    		$genre->name = $request->name;
            $genre->save();
    		return redirect(url("admin/".$slug."/genre"));                                                                   
    	} return abort(404);
    }
    public function delete($slug,$id){
    	if ($this->CheckAuth($slug)) {
    		$genre = $this->ConnectionObject("Genre",$slug);
    		if ($genre->where("id",$id)!=null) {
    			$genre->find($id)->delete();
    		}
    		return redirect(url("/admin/".$slug."/genre"));
    	}	
    	return abort(404);
    }
    public function search($slug){
    	if ($this->CheckAuth($slug)) {
    		$genres = $this->ConnectionObject("Genre",$slug)->all();
    		return view('admin.edit.genre_search',[
    			'slug'=>$slug,
    			'genres'=>$genres
    		]);
    	}return abort(404);
    }
    public function booksofgenre(Request $request,$slug){
    	if ($this->CheckAuth($slug)) {
    		$genre_id = $request->id;
    		$book = $this->ConnectionObject("Genre",$slug)->where("id",$genre_id)->books()->get();
            return json_encode($book);
    	}
    	return null;
    }
}
