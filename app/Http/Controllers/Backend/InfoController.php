<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
class InfoController extends Controller
{
    use LocationEmployee;
    
    function __construct(){
    	$this->loadServer();
    }
    public function server_info($slug){
    	if ($this->checkAuth($slug)) {
    		$server = config('database.connections.'.Auth::user()->connection);
    		return view('admin.server',[
    			'slug'=>$slug,
    			'server'=>$server
    		]);
    	}
    	return abort(404);
    }
    public function server_save(Request $request,$slug){
    	if ($this->checkAuth($slug)) {
    		$config = config('status');
    		$config[Auth::user()->connection] = $request->config;
    		$fb = fopen(config_path()."/status.php","w");
    		$str = " <?php return [";
    		$i = count($config); // 0 1 2 = 3
    		$k=0;
    		foreach ($config as $key => $value) {
    			$k++;
    			if ($i!=$k) {
    				$str.="\"".$key."\"=>\"".$value."\","; 
    			}else{
    				$str.="\"".$key."\"=>\"".$value."\""; 
    			}
    		}
    		$str = $str."];";
    		// print_r($str);
    		fwrite($fb,$str);
    		fclose($fb);
    		return redirect(url('/admin/'.$slug.'/server'));
    	}
    	return abort(404);
    }
}
