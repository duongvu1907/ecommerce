<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Requests\CustomerRequest;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Session;
use App\Branch;
use App\Customer;
use Hash;
class CustomerController extends Controller
{
	protected $server;
    use LocationServer;
    function __construct(){
    	$this->server =  $this->connectServer();
    }
    public function register(){
    	if(!is_null(Session::get("username"))){

    	}
    	$branches = $this->ConnectionObject($this->server,"Branch")->get();
    	return view('frontend.register',["branches"=>$branches]);
    }
    public function do_register(CustomerRequest $request){
    	// $customer = $this->ConnectionObject($this->server,"Customer")->get();
    	$validated = $request->validated();
    	$old_id = $this->ConnectionObject($this->server,"Customer")->orderBy("id","DESC")->first()->id;
        $new_id = $this->check_customer_id($this->server,$old_id);
    	$customer = $this->ConnectionObject($this->server,"Customer")->create([
            "id"=>$new_id,
    		"name"=>$request->name,
    		"address"=>$request->address,
    		"gender"=>$request->gender,
    		"phone"=>$request->phone,
    		"branch_id"=>$request->branch_id,
    		"username"=>$request->username,
    		"email"=>$request->email,
    		"password"=>Hash::make($request->password)
    	]);

    	return redirect(url('customer/login'));
    }
    public function login(){
    	if (!is_null(Session::get("username"))) {
    		return redirect(url("/"));
    	}
    	$branches = $this->ConnectionObject($this->server,"Branch")->get();
    	return view('frontend.login',["branches"=>$branches]);
    }
    public function do_login(Request $request){
    	$branch = $request->branch_id;
    	$choose = $request->choose;
    	$username = $request->unique;
    	$password = Hash::make($request->password);
    	$user = $this->ConnectionObject($this->server,"Customer")->where($choose,"LIKE",$username)->first();
    	if (is_null($user)) {
    		return redirect(url('customer/login'));
    	}
    	Session::put("username",[
    		"id"=>$user->id,
    		"username"=>$user->username,
    		"email"=>$user->email,
    	]);
    	return redirect(url("/"));
    }
    public function do_logout(){
    	if (!is_null(Session::get("username"))) {
    		Session::forget("username");
    	}
    	return redirect(url("/"));
    }
    public function profile(){
    	if (!is_null(Session::get("username"))) {
    		$user = $this->ConnectionObject($this->server,"Customer")->where("id",Session::get("username")["id"])->first();
    		$branches = $this->ConnectionObject($this->server,"Branch")->get();
    		return view("frontend.customer_profile",[
    			"user"=>$user,
    			"branches"=>$branches
    		]);
    	}
    	return redirect(url('customer/login'));
    }
    public function profile_save(Request $request){
    	if (!is_null(Session::get("username"))) {
    		$user = $this->ConnectionObject($this->server,"Customer")->where("id",Session::get("username")["id"])->first();
    		$user->name = $request->name;
    		$user->address = $request->address;
    		$user->gender = $request->gender;
    		$user->phone = $request->phone;
    		if (isset($request->phone) && $request->phone!="") {
    			$user->password = $request->phone;
    		}
    		$user->save();
    		return redirect(url('customer/profile'));
    	}
    	return redirect(url('customer/login'));
    }
    
}
