<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use App\Book;
use App\Discount;
use Auth;
class HomeController extends Controller
{
    protected $server;
    use LocationServer;
    public function __construct(){
         $this->server =  $this->connectServer();
    }
    public function index(){
        // if ($this->server != "sqlsrv"){
        //     $this->index_fake();
        // }else{
        // Session::forget("cart");
        // echo $this->server;
        // $this->loadNow("sqlsrv2");
        $branch_id =  $this->branch_id($this->server);
        $book_id = DB::connection($this->server)->select("SELECT TOP 10 book_id  From branches_books group by book_id Order by book_id DESC"); 
        $book_sell_id = DB::connection($this->server)->select("
                    SELECT TOP 10 orders_details.book_id ,sum(quantity) as qty
                    From orders_details
                    group by book_id
                    Order By qty
            ");
        $data_seller = array();
        foreach ($book_sell_id as $id) {
                // var_dump($id);
                array_push($data_seller,$this->ConnectionObject($this->server,"Book")->where("id",$id->book_id)->first() );
            }

        // $book = $this->ConnectionObject($this->server,"Book")::find($book_id)->get();
        // var_dump($book_id);
        $data = array();
        foreach ($book_id as $id) {
            // var_dump($id);
            array_push($data,$this->ConnectionObject($this->server,"Book")->where("id",$id->book_id)->first() );
        }
    	return view('frontend.home',[
            "books"=>$data,
            "book_sell"=>$data_seller
        ]);
        // }
    }

    public function index_fake(){
        $book=array();
        switch ($this->server) {
            case "sqlsrv2":

                    $book_id_DN = DB::connection($this->server)->select("
                        SELECT book_id FROM dbo.branches_books 
                        GROUP BY book_id ORDER BY book_id DESC
                        ");
                    $book_id_HCM = DB::connection($this->server)->select("
                        SELECT book_id FROM HCM.Store.dbo.branches_books 
                        GROUP BY book_id ORDER BY book_id DESC
                        ");
                    foreach ($book_id_DN as $book_id) {
                        array_push($data,$book_id->book_id);
                    }
                    foreach ($book_id_HCM as $book_id) {
                        array_push($data,$book_id->book_id);
                    }
                    $book_sell_id = DB::connection($this->server)->select("
                        SELECT TOP 10 dn.book_id ,sum(quantity) as qty
                        From orders_details dn inner join HCM.Store.dbo.orders_details hcm
                        ON dn.book_id=hcm.book_id 
                        group by dn.book_id
                        Order By qty
                      ");
                break;
            case "sqlsrv3":
                    $book_id_HCM = DB::connection($this->server)->select("
                        SELECT book_id FROM dbo.branches_books 
                        GROUP BY book_id ORDER BY book_id DESC
                        ");
                    foreach ($book_id_HCM as $book_id) {
                        array_push($data,$book_id->book_id);
                    }
            
        }
        $data = array_unique($data);
        $books = array();
        foreach ($data as $d) {
            array_push($book,$this->ConnectionObject($this->server,"Book")->where("id",$d)->first());
        }
        
        return view('frontend.home',[
            "books"=>$books,
            "book_sell"=>$data_seller
        ]);
    }
    public function get_genres(){
    	$genres = $this->ConnectionObject($this->server,"Genre")->get();
        $data = [];
        foreach ($genres as $genre) {
            array_push($data,[
                "id"=>$genre->id,
                "name"=>$genre->name
            ]);
        }
        return json_encode($data);
    }
    public function send(){
    	
    }
    public function cart_add(Request $request){
        $id = $request->product_id;
        $book = $this->ConnectionObject($this->server,"Book")->where("id",$id)->first();
        
        $dis = 0;
        $discount = is_null($book->discounts()->get())?array():$book->discounts()->get();
        foreach ($discount as $d) {
            $dis+=$d->value;
        }
        $dis = $dis==0?0:$dis;
        if (!is_null(Session::get("cart"))) {
            $data = Session::get("cart");
            $check = 0;
            foreach ($data as $cart) {
                if ($cart["id"]==$id) {
                    $cart["quantity"]=$cart["quantity"]+1;
                    $check++;

                }
            }
            if ($check==0) {
                array_push($data,[
                    "id"=>$book->id,
                    "name"=>$book->name,
                    "price_new"=>$dis==0?$book->price:$book->price-$book->price*$dis/100,
                    "price_old"=>$book->price,
                    "images"=>$book->images,
                    "discount"=>$dis,
                    "quantity"=>1
                ]);
            }
            
            Session::put("cart",$data);
        }else{
            Session::put("cart",[
                [
                    "id"=>$book->id,
                    "name"=>$book->name,
                    "price_new"=>$dis==0?$book->price:$book->price-$book->price*$dis/100,
                    "price_old"=>$book->price,
                    "images"=>$book->images,
                    "discount"=>$dis,
                    "quantity"=>1,
                ],
            ]);
        }
        return json_encode(Session::get("cart")) ;
        
    }
    public function cart_add_detail(Request $request){
        $id = $request->product_id;
        $book = $this->ConnectionObject($this->server,"Book")->where("id",$id)->first();
        
        $dis = 0;
        $discount = is_null($book->discounts()->get())?array():$book->discounts()->get();
        foreach ($discount as $d) {
            $dis+=$d->value;
        }
        $dis = $dis==0?0:$dis;
        if (!is_null(Session::get("cart"))) {
            $data = Session::get("cart");
            $check = 0;
            foreach ($data as $cart) {
                if ($cart["id"]==$id) {
                    $cart["quantity"]=$cart["quantity"]+1;
                    $check++;

                }
            }
            if ($check==0) {
                array_push($data,[
                    "id"=>$book->id,
                    "name"=>$book->name,
                    "price_new"=>$dis==0?$book->price:$book->price-$book->price*$dis/100,
                    "price_old"=>$book->price,
                    "images"=>$book->images,
                    "discount"=>$dis,
                    "quantity"=>1
                ]);
            }
            
            Session::put("cart",$data);
        }else{
            Session::put("cart",[
                [
                    "id"=>$book->id,
                    "name"=>$book->name,
                    "price_new"=>$dis==0?$book->price:$book->price-$book->price*$dis/100,
                    "price_old"=>$book->price,
                    "images"=>$book->images,
                    "discount"=>$dis,
                    "quantity"=>1,
                ],
            ]);
        }
        return redirect(url('/'));
        
    }
    public function search(Request $request){
        $genres = $this->ConnectionObject($this->server,"Genre")->get();
        $name = "%".$request->name."%";
        $books = null;
        if ($request->genre == 0) {
            $books =  $this->ConnectionObject($this->server,"Book")->where("name","LIKE",$name)->get();
        }else{
            $genre =  $this->ConnectionObject($this->server,"Genre")->where("id",$request->genre)->first();
            $books = $genre->books()->where("name","LIKE",$name)->get();
        }
        $branches = $this->ConnectionObject($this->server,"Branch")->get();
        return view('frontend.search',["books"=>$books,"branches"=>$branches,"genres"=>$genres]);


    }
    public function view_cart(){
         $branches = $this->ConnectionObject($this->server,"Branch")->get();
        $cart = is_null(Session::get("cart"))?array():Session::get("cart");
        return view("frontend.cart",["cart"=>$cart,"branches"=>$branches]);
    }
    public function check_out(){
        if (is_null(Session::get("username"))) {
            return redirect(url("customer/login"));
        }
        $cart = is_null(Session::get("cart"))?array():Session::get("cart");
        $branches = $this->ConnectionObject($this->server,"Branch")->get();
        $customer = $this->ConnectionObject($this->server,"Customer")->where("id",Session::get("username")["id"])->first();

        return view("frontend.check_out",[
            "cart"=>$cart,
            "branches"=>$branches,
            "customer"=>$customer
        ]);
    }
    public function remove_cart($id){
        if (!is_null(Session::get("cart"))) {
            // echo $id;
            $data = Session::get("cart");
            foreach ($data as $key => $value) {
                if ($value["id"]==$id) {
                    unset($data[$key]);
                }
            }
            // var_dump($data);
            Session::put("cart",$data);
            return redirect(url('/view-cart/'));
        }
        return abort(404);
    }
    public function check_out_save(Request $request){
        // var_dump($request->book_id);
        $this->ConnectionObject($this->server,"Order")->create([
            "customer_id"=>Session::get("username")["id"],
            "status"=>false
        ]);
        $order_id = $this->ConnectionObject($this->server,"Order")->orderBy("id","DESC")->first()->id; 
        for ($i = 0; $i <= count($request->book_id)-1 ; $i++) {
            $book = $this->ConnectionObject($this->server,"Book")->where("id",$request->book_id[$i])->first();
            $discount = 0;
            if (!is_null($book->discounts)) {
                foreach ($book->discounts as $d) {
                    $discount+=$d->value;
                }
            }
            // echo $this->server;
            // echo '<pre>';
            // var_dump([
            //     "order_id"=>$order->id,
            //     "quantity"=>$request->quantity[$i],
            //     "book_id"=>$request->book_id[$i],
            //     "branch_id"=>$request->branch_id[$i],
            //     "amount"=>$request->quantity[$i]*($book->price-$book->price*$discount/100)
            // ]);

            $order_detail = $this->ConnectionObject($this->server,"OrderDetail")->create([
                "order_id"=>$order_id,
                "quantity"=>$request->quantity[$i],
                "book_id"=>$request->book_id[$i],
                "branch_id"=>$request->branch_id[$i],
                "amount"=>$request->quantity[$i]*($book->price-$book->price*$discount/100)
            ]);
           // $order_detail->save();
        }
        Session::forget("cart");
        return redirect(url("/"));
    }
    public function do_search(Request $request){
        $name = "%".$request->name."%";
        $book_branch = array();
       $branches = $this->ConnectionObject($this->server,"Branch")->get();
        if ($request->branch_id != null) {
            $branch = $this->ConnectionObject($this->server,"Branch_Book")->where("branch_id",$request->branch_id)->get();
            // var_dump($branch);
            foreach ($branch  as $bb) {
                if (!is_null($this->ConnectionObject($this->server,"Book")->where("id",$bb->book_id)->where("name","LIKE",$name)->first())) {
                     array_push($book_branch,$this->ConnectionObject($this->server,"Book")->where("id",$bb->book_id)->where("name","LIKE",$name)->first());
                }
            }
        }
        // echo '<pre>';
        // var_dump($book_branch);
       return view('frontend.search',["books"=>$book_branch,"branches"=>$branches]);
    }
    public function detail($id){
        $book = $this->ConnectionObject($this->server,"Book")->where("id",$id)->first();
        $branches = $this->ConnectionObject($this->server,"Branch")->get();
        // var_dump($book->genres()->first()->name);
        $relate = $this->ConnectionObject($this->server,"Book")->where("id","<>",$book->id)->get();
        return view('frontend.detail',[
            "book"=>$book,
            "branches"=>$branches
        ]);
    }
    public function server_details(Request $request){
        $branch_id = $this->branch_id($this->server);
        return json_encode($this->ConnectionObject($this->server,"Branch")->where("id",$branch_id)->first()->name);
    }
}
