<?php
 namespace App\Http\Controllers\Frontend;
 /**
  * summary
  */
use DB;
 trait LocationServer
 {
     public function branch_id($server){
        
        if ($server=="sqlsrv") {
            return 1;
        }
        $id = explode("sqlsrv",$server);
        return $id[count($id)-1];
     }
     public function ConnectionObject($sqlsrv,$model)
     {
         $str = "App\\".$model;
         $obj = new $str;
         $obj->connection = $sqlsrv;
         $obj->setConnection($sqlsrv);
         return $obj;
     }
     public function server_status($server){
     	return  config("status.".$server);
     }
     public function updateStatus($array){
     	$str = "<?php return ".$array.";";
     	$fb = fopen(config_path()."/"."status.php","w");
     	fwrite($fb,$str);
     	fclose($fb);

     }
     public function connectServer(){
     	$server = "sqlsrv";
        $status = $this->server_status($server);
    	if ($status=="maintenance") {

            $server = "sqlsrv2";
            $status = $this->server_status($server);
        }else{
        	if ($this->checkStatusServer($server)) {
        		return $server;
        	}else{
                
        		$this->updateStatus("[\"sqlsrv\"=>\"maintenance\",\"sqlsrv2\"=>\"running\",\"sqlsrv3\"=>\"running\"]");
        		$server = 'sqlsrv2';
        		$status = $this->loadNow($server);
        	}
        }
        if ($status=="maintenance") {

            $server = "sqlsrv3";
            $status = $this->server_status($server);
        }else{
        	if ($this->checkStatusServer($server)) {
        		return $server;
        	}else{
        		$this->updateStatus("[\"sqlsrv\"=>\"maintenance\",\"sqlsrv2\"=>\"running\",\"sqlsrv3\"=>\"running\"]");
        		$server = 'sqlsrv3';
        		$status = $this->loadNow($server);
        	}
        }
        if ($status=="maintenance") {
            return abort(500);
        }else{
        	if ($this->checkStatusServer($server)) {
        		return $server;
        	}else{
        		$this->updateStatus("['sqlsrv'=>'maintenance','sqlsrv2'=>'maintenance','sqlsrv3'=>'maintenance']");
        		return abort(500);
        	}
        }

        return $server;
     }
     public function checkStatusServer($server){
     	if (DB::connection($server)->getPdo()) {
     		return true;
     	}
     	return false;
     }
     public function  loadNow($server){
        $fb = fopen(config_path()."/status.php","r");
        $arr =  fread($fb,filesize(config_path()."/status.php"));
        $arr = str_replace(" <?php return [", "", $arr);
        $arr = str_replace("];", "", $arr);
        $array_server = array();
        $server_total = explode(",",$arr);;
        // echo '<pre>';
        // var_dump($server_total);
        foreach ($server_total as $s) {
            $srv = explode("=>",$s);
            // var_dump($srv);
            $array_server[str_replace("\"","",$srv[0])] =str_replace("\"","",$srv[1]);
        }
        // var_dump($array_server);
        fclose($fb);
        return $array_server[$server];
     }
      public function check_customer_id($server,$old_id){
            $new_id = $old_id+1;
            $check = DB::connection($server)->select("
                DECLARE @value INT 
                EXEC @value = check_id_customer @customer_id = ".$new_id."
                SELECT @value
            ");
            if ($check==1) {
                $this->check_id($server,$new_id);
            }else{
                return $new_id;
            }
        }
 }
