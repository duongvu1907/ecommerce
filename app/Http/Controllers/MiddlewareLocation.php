<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Branch;
class MiddlewareLocation extends Controller
{
    public function authenticate($id){
    	
    	$branch_id = isset(Auth::user()->branch_id)?Auth::user()->branch_id:0;
    	$branch = Branch::where('id',$id)->first();
    	if ($id == $branch_id) {
    		$name = $branch->slug;
    		// $this->DBConnection($branch->name);
    		return redirect(url("admin/dashboard/$name/"));
    	}else{
    		$email = $this->FailLocation();
    		return view("admin.faillocation",["branch"=>$branch,"email"=>$email]);
    	}
    }
    public function DBConnection($name){
    	
    }
    public function FailLocation(){
    	$email = isset(Auth::user()->email)?Auth::user()->email:"";
    	Auth::logout();
    	return $email;
    }
}
