<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = "orders" ;

    protected $fillable = [
    	"customer_id","status"
    ];
    public function orders_details(){
    	return $this->hasMany("App\OrderDetail","order_id");
    }
    public function customer(){
    	return $this->belongsTo("App\Customer");
    }
}
