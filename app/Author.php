<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $table = "authors";
    protected $connection = "";
    protected $fillable = [
    	"name","address","job"
    ];
    
    public function books(){
    	return $this->hasMany("App\Book","author_id");
    }
    public  function setConnect($sqlsrv){
    	$this->connection = $sqlsrv;
    }
}
