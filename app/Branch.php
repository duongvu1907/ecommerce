<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable = [
    		"name","address"
    ];
    public function user(){
    	return $this->hasMany('App\User','branch_id');
    }
    public function branches_books(){
    	return $this->hasMany("App\Branch_Book","branch_id");
    }
    public function customers(){
    	return $this->hasMany("App\Customer","branch_id");
    }
    public function orders_details(){
    	return $this->hasMany("App\OrderDetail","branch_id");
    }
}
