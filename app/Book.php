<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = "books";
    protected $fillable = [
    	"name","edition","author_id","public","price","genre_id","publisher_id","images"
    ] ;
    public function author(){
    	return $this->belongsTo("App\Author");
    }
    public function genres(){
    	return $this->belongsToMany("App\Genre","genres_books","book_id","genre_id");
    }
    public function discounts(){
    	return $this->belongsToMany("App\Discount","discounts_books","discount_id","book_id");
    }
    public function publisher(){
    	return $this->belongsTo("App\Publisher");
    }
    public function branches_books(){
    	return $this->hasMany("App\Branch_Book","book_id");
    }
    public function reviews(){
    	return $this->hasMany("App\Review");
    }
    public function orders_details(){
    	return $this->hasMany("App\OrderDetail","book_id");
    }
}
