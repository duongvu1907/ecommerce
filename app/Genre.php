<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $table = "Genres";

    protected $fillable = [
    	"name"
    ];
    public $timestamp = false;
    public function books(){
    	return $this->belongsToMany("App\Book","genres_books","genre_id","book_id");
    }
}
