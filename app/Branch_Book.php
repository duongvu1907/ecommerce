<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch_Book extends Model
{
    protected $table = "branches_books";

    protected $fillable = [
    	"book_id","branch_id","quantity"
    ];
    public function branch(){
    	return $this->belongsTo("App\Branch");
    }
    public function book(){
        return $this->belongsTo("App\Book");
    }
}
