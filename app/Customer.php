<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = "customers";

    protected $fillable = [
    	"name","username","email","phone","address","password","gender","branch_id"
    ];
    public function branch(){
    	return $this->belongsTo("App\Branch");
    }
    public function reviews(){
    	return $this->hasMany("App\Review","customer_id");
    }
    public function orders(){
    	return $this->hasMany("App\Order","customer_id");
    }
    public function discounts(){
    	return $this->belongsToMany("App\Discount","discounts_customers","customer_id","discount_id");
    }
}
