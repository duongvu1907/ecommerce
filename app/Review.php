<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = "reviews";

    protected $fillable = [
    	"book_id","customer_id","rate","content"
    ];

    public function customer(){
    	return $this->belongsTo("App\Customer");
    }
    public function review(){
    	return $this->belongsTo("App\Review");
    }
}
