<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCustomers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table){
            $table->bigIncrements('id');
            $table->string("name");
            $table->string("username")->unique();
            $table->string("gender");
            $table->string("address");
            $table->string("phone")->unique();
            $table->string("email")->unique();
            $table->string("password");
            $table->bigInteger("branch_id");
            $table->foreign("branch_id")->references("id")->on("branches");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
