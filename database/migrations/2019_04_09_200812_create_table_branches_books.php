<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBranchesBooks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches_books', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger("book_id");
            $table->bigInteger("branch_id");
            $table->foreign("book_id")->references("id")->on("books");
            $table->foreign("branch_id")->references("id")->on("branches");
            $table->integer("quantity");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches_books');
    }
}
