<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDiscountsBooks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discounts_books', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger("book_id");
            $table->bigInteger("discount_id");
            $table->foreign("book_id")->references("id")->on("books");
            $table->foreign("discount_id")->references("id")->on("discounts");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discounts_books');
    }
}
