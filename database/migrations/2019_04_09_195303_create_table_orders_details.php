<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrdersDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("quantity");
            $table->bigInteger("book_id");
            $table->bigInteger("order_id");
            $table->bigInteger("branch_id");
            $table->foreign("book_id")->references("id")->on("books");
            $table->foreign("order_id")->references("id")->on("orders");
            $table->foreign("branch_id")->references("id")->on("branches");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_details');
    }
}
