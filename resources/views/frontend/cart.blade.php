@extends("frontend.layout")
@section('content')
<section style="margin-top: 100px">
	<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4>View Cart</h4>
		</div>
		<div class="panel-body">
			<form action="{{url('/check-out/save')}}" method="post">
			<table class="table table-hover">
				<tr>
					<th></th>
					<th>Name</th>
					<th>Quantity</th>
					<th>Price</th>
					<th>Discount</th>
					<th></th>
				</tr>
			<?php $total=0; ?>
			<?php foreach ($cart as $c): ?>
				<?php $total+=$c['price_new']; ?>
				<tr>
					<td><img src="{{asset('upload/books/'.$c['images'])}}" alt="" width="130px"></td>
					<td>{{$c['name']}}</td>
					<td>
						<input type="number" value="{{$c['quantity']}}" class="quantity">
					</td>
					<td class="price">{{$c['price_new']}}</td>
					
					<td class="discount">{{$c['discount']}}</td>
					<td>
						<a href="{{url('/view-cart/delete/'.$c['id'])}}"><span class="fa fa-trash text-danger remove"></span></a>
					</td>
				</tr>	
			<?php endforeach ?>
			</table>
			</form>
		</div>
		<div class="row">
			<div class="col-md-4">
				
			</div>
			<div class="col-md-4">
				Total  : <span class="total">
					
				{{$total}}
				</span>
			</div>
		</div>
	</div>
</div>
</section>
@endsection
@section('script')
<script type="text/javascript">
		
		$(".quantity").change(function(event) {
			var quantity = $(".quantity");
			var price = $(".price");
			var total = 0;  
			// var discount = $(".discount")
			for (var i = 0; i < quantity.length; i++) {
				
				// discount.text();
				var pr = parseFloat(price.eq(i).text())*parseInt(quantity.eq(i).val());
				total+= pr;
				// console.log(price.eq(i).;
			}

			$(".total").text(total);
		});

	</script>
@endsection