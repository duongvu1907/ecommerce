@extends("frontend.layout")
@section('content')
<section style="margin-top: 50px;">
	<div class="container">
		<div class="panel panel-primary">
			<div class="panel-heading">
				Register 
			</div>
			<div class="panel-body">
				<form action="{{url('/customer/register')}}" method="post">
					@csrf
				<div class="form-group">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-8">
							<label>Name</label>
						<input type="text" class="form-control" name="name" required="">
						</div>
						<div class="col-md-2"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-4">
							<label>Address</label>
							<input type="text" class="form-control" name="address" required="">
						</div>
						<div class="col-md-4">
							<label>Gender</label>
							<select class="form-control" name="gender">
								<option value="male">Male</option>
								<option value="female">Female</option>
								<option value="other">Other</option>
							</select>
						</div>
						<div class="col-md-2"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-8">
							<label>Phone</label>
						<input type="number" class="form-control" name="phone" required="">
						</div>
						<div class="col-md-2"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-8">
							<label>Branch</label>
							<select class="form-control" name="branch_id" required="">
								<option value=""></option>
								<?php foreach ($branches as $branch): ?>
									<option value="{{$branch->id}}">{{$branch->name}}</option>
								<?php endforeach ?>
							</select>
						</div>
						<div class="col-md-2"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-4">
							<label>Username</label>
						<input type="text" class="form-control" name="username" required="">
						</div>
						<div class="col-md-4">
							<label>Email</label>
						<input type="email" class="form-control" name="email" required="">
						</div>
						<div class="col-md-2"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-4">
							<label>Password</label>
							<input type="password" name="password" class="form-control" required="">
						</div>
						<div class="col-md-2"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-4">
							<button class="btn btn-success">Register</button>
						</div>
						<div class="col-md-2"></div>
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
</section>
@endsection