@extends("frontend.layout")
@section('content')
<div class="section">
	<div class="container">
		<div class="row">
			<form action="{{url('/search/details/')}}" method="post">
				@csrf
				<div class="col-md-3">
					<input type="text" name="name" placeholder="Name ***" class="form-control" required="">
				</div>
				
				<div class="col-md-3">
					<select name="branch_id" class="form-control">
						<?php foreach ($branches as $branch): ?>
							<option value="{{$branch->id}}">{{$branch->name}}</option>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-md-3">
					<button class="btn btn-success">Search</button>
				</div>
			</form>
		</div>

		<div class="row" style="margin-top: 100px">
			<?php foreach ($books as $book): ?>				
			
			<div class="col-md-4">
				<div class="product">
					<div class="product-img">
						<img src="{{asset('/upload/books/'.$book->images)}}" alt="">
						<div class="product-label">
							<?php 
							$time = date("Y-m-d h:i:s",time());
							$total = 0;
							$discount=array();
							if(!is_null($book->discounts()->get())){
								$discount = $book->discounts()->where("expiry_at",">=",$time)->get();
															// var_dump($discount);
							} ?>
							<?php foreach ($discount as $d): ?>
								<?php $total+= $d->value;?>
								<span class="sale">
									-{{$d->value}}%
								</span>
							<?php endforeach ?>
							<span class="sale">-<!-- 30 --></span>
							
						</div>
					</div>
					<div class="product-body">
						<input type="number" hidden="" class="book_id" value="{{$book->id}}" >
						<p class="product-category">{{$book->genres()->first()->name}}</p>
						<h3 class="product-name"><a href="{{url('/books/detail/'.$book->id)}}">{{$book->name}}</a></h3>
						<h4 class="product-price">
							@if($total!=0)
							{{number_format($book->price-$book->price*$total/100)}}
							<del class="product-old-price">{{number_format($book->price,1)}}</del>
							@else
							{{number_format($book->price,1)}}
							<del class="product-old-price"></del>
							@endif
						</h4>
						<div class="product-rating">

						</div>
						<div class="product-btns">
							<button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">add to wishlist</span></button>
							<button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">add to compare</span></button>
							<button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">quick view</span></button>
						</div>
					</div>
					<div class="add-to-cart">
						<button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>
					</div>
				</div>
			</div>
			<?php endforeach ?>
		</div>
	</div>
</div>
@endsection