<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		 <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

		<title>ABC Book Store - Customer is .......</title>

		<!-- Google font -->
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

		<!-- Bootstrap -->
		<link type="text/css" rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css')}}"/>

		<!-- Slick -->
		<link type="text/css" rel="stylesheet" href="{{ asset('frontend/css/slick.css')}}"/>
		<link type="text/css" rel="stylesheet" href="{{ asset('frontend/css/slick-theme.css')}}"/>

		<!-- nouislider -->
		<link type="text/css" rel="stylesheet" href="{{ asset('frontend/css/nouislider.min.css')}}"/>

		<!-- Font Awesome Icon -->
		<link rel="stylesheet" href="{{ asset('frontend/css/font-awesome.min.cs')}}s">

		<!-- Custom stlylesheet -->
		<link type="text/css" rel="stylesheet" href="{{ asset('frontend/css/style.css')}}"/>
		<style type="text/css">
			.alert-cart{
				width: 300px;position: fixed;bottom:-500px;right: 20px;z-index: 9999;
			}
			.alert-cart.active{
				 bottom: 20px;
				 transition: 1s ease;
			}
			.product-img {
			    width: 235px;
			    text-align: center;
			    height: 262px;
			    overflow: hidden;
			    text-align: center;
			}
		</style>

    </head>
	<body>
		<!-- HEADER -->
		<header>
			<!-- TOP HEADER -->
			<div id="top-header">
				<div class="container">
					<ul class="header-links pull-left">
						<!-- <li><a href="#"><i class="fa fa-phone"></i> +021-95-51-84</a></li>
						<li><a href="#"><i class="fa fa-envelope-o"></i> email@email.com</a></li>
						<li><a href="#"><i class="fa fa-map-marker"></i> 1734 Stonecoal Road</a></li> -->
					</ul>
					<ul class="header-links pull-right">
							@if(!is_null(Session::get('username')))
									<li><a href="{{url('/customer/profile')}}"><i class="fa fa-user-o"></i>
									{{Session::get("username")["username"]}}</a></li>
									<li><a href="{{url('/customer/logout')}}"><i class="fa fa-sign-out-alt"></i>Logout</a></li>
							@else
							<li><a href="{{url('/customer/login')}}"><i class="fa fa-user-o"></i>
								Login</a></li>
							<li><a href="{{url('/customer/register')}}"><i class="fa a-sign-out-alt"></i>
								Register</a></li>
							@endif

						
					</ul>
				</div>
			</div>
			<!-- /TOP HEADER -->

			<!-- MAIN HEADER -->
			<div id="header">
				<!-- container -->
				<div class="container">
					<!-- row -->
					<div class="row">
						<!-- LOGO -->
						<div class="col-md-3">
							<div class="header-logo">
								<a href="{{url('/')}}" class="logo">
									<img src="{{asset('logo.png')}}" alt="" width="124px">
								</a>
							</div>
						</div>
						<!-- /LOGO -->
						
						<!-- SEARCH BAR -->
						<div class="col-md-6">
							<div class="header-search">
								<form action="{{url('/search')}}" method="get">
									<select class="input-select genres-sl" name="genre">
										<option value="0">All Categories</option>
									</select>
									<input class="input" placeholder="Search here" name="name" required="">
									<button class="search-btn" type="submit">Search</button>
								</form>
							</div>
						</div>
						<!-- /SEARCH BAR -->

						<!-- ACCOUNT -->
						<div class="col-md-3 clearfix">
							<div class="header-ctn">
								<!-- Wishlist -->
								<div>
									<a href="#">
										<i class="fa fa-heart-o"></i>
										<span>Your Wishlist</span>
									@if(!is_null(Session::get("wish")))
										<div class="qty">2</div>
									@endif
									</a>
								</div>
								<!-- /Wishlist -->

								<!-- Cart -->
								<div class="dropdown">
									<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
										<i class="fa fa-shopping-cart"></i>
										<span>Your Cart</span>
										@if(!is_null(Session::get("cart")))
										<div class="qty">{{count(Session::get("cart"))}}</div>
										@else
										<div class="qty"></div>
										@endif
									</a>
									<div class="cart-dropdown">
										<div class="cart-list">
											@if(!is_null(Session::get("cart")))
												<?php $total = 0; ?>
											<?php foreach (Session::get("cart") as $cart): ?>
											<?php $total+=$cart["price_new"];?>
											<div class="product-widget">
												<div class="product-img">
													<img src="{{asset('upload/books/'.$cart['images'])}}" alt="">
												</div>
												<div class="product-body">
													<h3 class="product-name"><a href="#">{{$cart["name"]}}</a></h3>
													<h4 class="product-price"><span class="qty-c">{{$cart["quantity"]}}x</span>{{$cart["price_new"]}} <del><span style="color:lavender;">{{$cart["price_old"]}}</span></del></h4>
												</div>
												<a href="{{url('/view-cart/delete/'.$cart['id'])}}"><button class="delete"><i class="fa fa-close"></i></button></a>
											</div>

											<?php endforeach ?>
											@endif
											
										</div>
										<div class="cart-summary">
											@if(!is_null(Session::get("cart")))
											<small>{{count(Session::get("cart"))}} Item(s) selected</small>
											<h5>SUBTOTAL:<?php echo $total ?></h5>
											@endif
										</div>
										<div class="cart-btns">
											<a href="{{url('/view-cart')}}">View Cart</a>
											<a href="{{url('/check-out')}}">Checkout  <i class="fa fa-arrow-circle-right"></i></a>
										</div>
									</div>
								</div>
								<!-- /Cart -->

								<!-- Menu Toogle -->
								<div class="menu-toggle">
									<a href="#">
										<i class="fa fa-bars"></i>
										<span>Menu</span>
									</a>
								</div>
								<!-- /Menu Toogle -->
							</div>
						</div>
						<!-- /ACCOUNT -->
					</div>
					<!-- row -->
				</div>
				<!-- container -->
			</div>
			<!-- /MAIN HEADER -->
		</header>
		<!-- /HEADER -->

		<!-- NAVIGATION -->
		<nav id="navigation">
			<!-- container -->
			<div class="container">
				<!-- responsive-nav -->
				<div id="responsive-nav">
					<!-- NAV -->
					<ul class="main-nav nav navbar-nav">
						<li class="active"><a href="/">Home</a></li>
						<li class="server"><a href="/"></a></li>
						
					</ul>
					<!-- /NAV -->
				</div>
				<!-- /responsive-nav -->
			</div>
			<!-- /container -->
		</nav>
		<!-- /NAVIGATION -->

		<!-- SECTION -->
		@yield('content')
		<!-- FOOTER -->
		<footer id="footer">

			<div id="bottom-footer" class="section">
				<div class="container">
					<!-- row -->
					<div class="row">
						<div class="col-md-12 text-center">
							<ul class="footer-payments">
								<li><a href="#"><i class="fa fa-cc-visa"></i></a></li>
								<li><a href="#"><i class="fa fa-credit-card"></i></a></li>
								<li><a href="#"><i class="fa fa-cc-paypal"></i></a></li>
								<li><a href="#"><i class="fa fa-cc-mastercard"></i></a></li>
								<li><a href="#"><i class="fa fa-cc-discover"></i></a></li>
								<li><a href="#"><i class="fa fa-cc-amex"></i></a></li>
							</ul>
							<span class="copyright">
								<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
								Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
							</span>
						</div>
					</div>
						<!-- /row -->
				</div>
				<!-- /container -->
			</div>
			<!-- /bottom footer -->
		</footer>
		<div class="alert alert-success alert-cart">
			
		</div>
		<!-- /FOOTER -->
			
		<!-- jQuery Plugins -->
		<script src="{{ asset('frontend/js/jquery.min.js')}}"></script>
		<script src="{{ asset('frontend/js/bootstrap.min.js')}}"></script>
		<script src="{{ asset('frontend/js/slick.min.js')}}"></script>
		<script src="{{ asset('frontend/js/nouislider.min.js')}}"></script>
		<script src="{{ asset('frontend/js/jquery.zoom.min.js')}}"></script>
		<script src="{{ asset('frontend/js/main.js')}}"></script>

		<script>
				$.ajax({
				url: "<?php echo url('/api/server') ?>",
				type: 'POST',
				dataType: 'json',
				data: {_token:"<?php echo csrf_token() ?>"},
				success:function(data){
					
					$(".server a").text(data);
				}
				});
				$.ajax({
				url: "<?php echo url('/api/genres') ?>",
				type: 'POST',
				dataType: 'json',
				data: {_token:"<?php echo csrf_token() ?>"},
				success:function(data){
					$.each(data, function(index, val) {
						var option = "<option value="+val.id+">"+val.name+"</option>";
						 $(".genres-sl").append(option);
					});
				}
			})
			
		</script>
		<script type="text/javascript">
	$(".add-to-cart-btn").click(function(event) {
		
		var parent = $(this).parent().parent().find('.product-body');
		var product_id = parent.find('input.book_id').val();
		console.log(product_id);
		var product_name = parent.find('.product-name').find('a').text();
		var product_price = parent.find('.product-price').text();
		console.log(product_id);
		$.ajax({
			url: "<?php echo url('/cart/add') ?>",
			type: 'POST',
			dataType: 'json',
			data: {"product_id":product_id,product_name:product_name,product_price:product_price,_token:"<?php echo csrf_token() ?>"},
			success:function(data){
				$(".cart-list").find('.product-widget').remove();
				var count = data.length;
				$.each(data, function(index, val) {
					var img = "<?php echo asset('/') ?>"+"upload/books/"+val["images"]
					 var product = "<div class='product-widget'><div class='product-img'><img src='"+img+"' alt=''></div><div class='product-body'><h3 class='product-name'><a href='#'>"+val["name"]+"</a></h3><h4 class='product-price'><span class='qty-c'>"+val["quantity"]+"x </span>"+val["price_new"]+"<del><span style='color:lavender;'>"+val["price_old"]+"</span></del></h4></div><button class='delete'><i class='fa fa-close'></i></button></div>";

					$(".cart-list").append(product);
				});
				$(".qty").text(count);
				$(".alert-cart").html("<p Add Cart : >"+product_name+" <span class='fa fa-check'></span></p>");
				$(".alert-cart").addClass('active');
				setTimeout(function(){
					$(".alert-cart").removeClass('active');
				},5000);
			}
		})
		
	});
</script>
		@yield('script')
	</body>
</html>
