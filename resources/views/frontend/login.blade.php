@extends("frontend.layout")
@section('content')
<section style="margin-top: 50px;">
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				Login 
			</div>
			<div class="panel-body">
				<form action="{{url('customer/login')}}" method="post">
					@csrf
				<div class="form-group">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-8">
							<label>Login by</label>
							<select name="choose" required="">
								<option value=""></option>
								<option value="username">Username</option>
								<option value="email">Email</option>
							</select>
						</div>
						<div class="col-md-2"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-8">
							<label>Username / Email</label>
							<input type="text" class="form-control" name="unique" required="">
						</div>
						
						<div class="col-md-2"></div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-8">
							<label>Password</label>
							<input type="password" class="form-control" name="password" required="">
						</div>
						
						<div class="col-md-2"></div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-8">
							<label>Branch</label>
							<select class="form-control" name="branch_id" required="">
								<option value=""></option>
								<?php foreach ($branches as $branch): ?>
									<option value="{{$branch->id}}">{{$branch->name}}</option>
								<?php endforeach ?>
							</select>
						</div>
						<div class="col-md-2"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-8">
							<button class="btn btn-primary">Login</button>
							</div>
						<div class="col-md-2"></div>
					</div>
				</div>
				
				</form>
			</div>
		</div>
	</div>
</section>
@endsection