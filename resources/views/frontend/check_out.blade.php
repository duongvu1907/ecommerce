@extends("frontend.layout")
@section('content')
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					
					
						<ul style="padding: 50px">
							<li>{{$customer->name}}</li>
							<li>{{$customer->address}}</li>
							<li>{{$customer->email}}</li>
							<li>{{$customer->phone}}</li>
							<li>{{$customer->branch->name}}</li>
						</ul>
					
				</div>
				<div class="col-md-8">
					<form action="{{url('/check-out/save')}}" method="post">
						@csrf
						<table class="table table-hover">
							<tr>
								<th></th>
								<th>Name</th>
								<th>Quantity</th>
								<th>Price</th>
								<th>Location</th>
								<th>Discount</th>
								<th></th>
							</tr>
						<?php $total=0; ?>
						<?php foreach ($cart as $c): ?>
							<?php $total+=$c['price_new']; ?>
							<tr>
								<input type="number" name="book_id[]" hidden="" value="{{$c['id']}}">
								<td><img src="{{asset('upload/books/'.$c['images'])}}" alt="" width="130px"></td>
								<td>{{$c['name']}}</td>
								<td>
									<input type="number" value="{{$c['quantity']}}" name="quantity[]" class="quantity">
								</td>
								<td class="price">{{$c['price_new']}}</td>
								<td class="branch">
									<select name="branch_id[]">
										<?php foreach ($branches as $branch): ?>
											<option value="{{$branch->id}}" @if($branch->id==$customer->branch->id) selected @endif>{{$branch->name}}</option>
										<?php endforeach ?>
									</select>
								</td>
								<td class="discount">{{$c['discount']}}</td>
								<td>
									<a href="{{url('/view-cart/delete/'.$c['id'])}}"><span class="fa fa-trash text-danger remove"></span></a>
								</td>
							</tr>	
						<?php endforeach ?>
						</table>
						<button class="btn btn-success" type="submit">Buy</button>
						</form>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				
			</div>
			<div class="col-md-4">
				<h2>Total  : <span class="total">
					
				{{$total}}</h2>
				</span>
			</div>
		</div>
	</section>
@endsection
@section('script')
<script type="text/javascript">
		
		$(".quantity").change(function(event) {
			var quantity = $(".quantity");
			var price = $(".price");
			var total = 0;  
			var discount = $(".discount")
			for (var i = 0; i < quantity.length; i++) {
				if (discount.eq(i).text()=="") {
					dis=0;
				}else{
					dis = parseInt(discount.eq(i).text());
				}
				// discount.text();
				var pr = parseFloat(price.eq(i).text())*parseInt(quantity.eq(i).val());
				total+= pr;
				// console.log(price.eq(i).;
			}

			$(".total").text(parseInt(total));
		});

	</script>
@endsection