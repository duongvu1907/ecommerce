@extends('frontend.layout')
@section('content')
<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">

					<!-- section title -->
					<div class="col-md-12">
						<div class="section-title">
							<h3 class="title">New Books</h3>
							
						</div>
					</div>
					<!-- /section title -->

					<!-- Products tab & slick -->
					<div class="col-md-12">
						<div class="row">
							<div class="products-tabs">
								<!-- tab -->
								<div id="tab1" class="tab-pane active">
									<div class="products-slick" data-nav="#slick-nav-1">
										<?php foreach ($books as $book): ?>
											
										
										<div class="product">
											<div class="product-img">
												<img src="{{ asset('upload/books/'.$book->images)}}" alt="">
												<div class="product-label">
													
														<?php 
														$time = date("Y-m-d h:i:s",time());
														$total = 0;
														// $discount=array();
														$bb = DB::table("discounts_books")->where("book_id",$book->id)->get();
														$dis = array();
														foreach ($bb as $b) {
															if (!is_null(DB::table("discounts")->where("id",$b->discount_id)->where("expiry_at",">=",$time)->first())) {
																
															array_push($dis,DB::table("discounts")->where("id",$b->discount_id)->where("expiry_at",">=",$time)->first());
															}
														}
														// if(!is_null($book->discounts()->get())){
														// 	$discount = $book->discounts()->where("expiry_at",">=",$time)->get();
															
														// } 
														?>
													<?php foreach ($dis as $d): ?>
														<?php $total+= $d->value;?>
														<span class="sale">
															-{{$d->value}}%
														</span>
													<?php endforeach ?>
													
												</div>
											</div>
											<div class="product-body">
												<input type="number" hidden="" class="book_id" value="{{$book->id}}" >
												<p class="product-category">{{$book->genres()->first()->name}}</p>
												<h3 class="product-name"><a href="{{url('/books/detail/'.$book->id)}}">{{$book->name}}</a></h3>
												<h4 class="product-price">
													@if($total!=0)
													{{number_format($book->price-$book->price*$total/100)}}
													<del class="product-old-price">{{number_format($book->price,1)}}</del>
													@else
													{{number_format($book->price,1)}}
													<del class="product-old-price"></del>
													@endif
												</h4>
												<div class="product-rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
												</div>
												<div class="product-btns">
													<button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">add to wishlist</span></button>
													<button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">add to compare</span></button>
													<button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">quick view</span></button>
												</div>
											</div>
											<div class="add-to-cart">
												<button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>
											</div>
										</div>
										<!-- /product -->
										<?php endforeach ?>
									</div>
									<div id="slick-nav-1" class="products-slick-nav"></div>
								</div>
								<!-- /tab -->
							</div>
						</div>
					</div>
					<!-- Products tab & slick -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
<!-- 
	Top Selling ==============================------->
	<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">

					<!-- section title -->
					<div class="col-md-12">
						<div class="section-title">
							<h3 class="title">Top selling</h3>
							<div class="section-nav">
								
							</div>
						</div>
					</div>
					<!-- /section title -->

					<!-- Products tab & slick -->
					<div class="col-md-12">
						<div class="row">
							<div class="products-tabs">
								<!-- tab -->
								<div id="tab2" class="tab-pane fade in active">
									<div class="products-slick" data-nav="#slick-nav-2">
										<?php foreach ($book_sell as $book): ?>
											
										
										<div class="product">
											<div class="product-img">
												<img src="{{ asset('upload/books/'.$book->images)}}" alt="">
												<div class="product-label">
													
														<?php 
														$time = date("Y-m-d h:i:s",time());
														$total = 0;
														$discount=array();
														if(!is_null($book->discounts()->get())){
															$discount = $book->discounts()->where("expiry_at",">=",$time)->get();
															// var_dump($discount);
														} ?>
													<?php foreach ($discount as $d): ?>
														<?php $total+= $d->value;?>
														<span class="sale">
															-{{$d->value}}%
														</span>
													<?php endforeach ?>
													
												</div>
											</div>
											<div class="product-body">
												<input type="number" hidden="" class="book_id" value="{{$book->id}}" >
												<p class="product-category">{{$book->genres()->first()->name}}</p>
												<h3 class="product-name"><a href="{{url('/books/detail/'.$book->id)}}">{{$book->name}}</a></h3>
												<h4 class="product-price">
													@if($total!=0)
													{{number_format($book->price-$book->price*$total/100)}}
													<del class="product-old-price">{{number_format($book->price,1)}}</del>
													@else
													{{number_format($book->price,1)}}
													<del class="product-old-price"></del>
													@endif
												</h4>
												<div class="product-rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
												</div>
												<div class="product-btns">
													<button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">add to wishlist</span></button>
													<button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">add to compare</span></button>
													<button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">quick view</span></button>
												</div>
											</div>
											<div class="add-to-cart">
												<button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>
											</div>
										</div>
										<!-- /product -->
										<?php endforeach ?>
									</div>
									<div id="slick-nav-2" class="products-slick-nav"></div>
								</div>
								<!-- /tab -->
							</div>
						</div>
					</div>
					<!-- /Products tab & slick -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		
<!--------	Top Selling ==============================
-->
@endsection

@section('script')
<script type="text/javascript">
	$(".delete").click(function(event) {
		$
	});
</script>

@endsection