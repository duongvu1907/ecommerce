@extends('frontend.layout')
@section('content')
	<section>
		<div class="container">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4>Edit Profile</h4>
				</div>
				<div class="panel-body">
					<form action="{{url('/customer/profile/save')}}" method="post">
					@csrf
				<div class="form-group">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-8">
							<label>Name</label>
						<input type="text" class="form-control" value="{{$user->name}}" name="name" required="">
						</div>
						<div class="col-md-2"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-4">
							<label>Address</label>
							<input type="text" class="form-control" value="{{$user->address}}" name="address" required="">
						</div>
						<div class="col-md-4">
							<label>Gender</label>
							<select class="form-control" name="gender">
								<option value="male"  @if($user->gender=="male") selected @endif >Male</option>
								<option value="female"  @if($user->gender=="female") selected @endif >Female</option>
								<option value="other"  @if($user->gender=="othe") selected @endif >Other</option>
							</select>
						</div>
						<div class="col-md-2"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-8">
							<label>Phone</label>
						<input type="number" class="form-control" value="{{$user->phone}}" name="phone" required="">
						</div>
						<div class="col-md-2"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-8">
							<label>Branch</label>
							<select class="form-control" name="branch_id" required="">
								<option value=""></option>
								<?php foreach ($branches as $branch): ?>
									<option value="{{$branch->id}}" @if($user->branch_id==$branch->id) selected @endif>{{$branch->name}}</option>
								<?php endforeach ?>
							</select>
						</div>
						<div class="col-md-2"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-4">
							<label>Username</label>
						<input type="text" class="form-control" name="username" value="{{$user->username}}" disabled="">
						</div>
						<div class="col-md-4">
							<label>Email</label>
						<input type="email" class="form-control" name="email" value="{{$user->email}}" disabled="" required="">
						</div>
						<div class="col-md-2"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-4">
							<label>Password</label>
							<input type="password" name="password" class="form-control" >
						</div>
						<div class="col-md-2"></div>
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-4">
							<button class="btn btn-success">Edit</button>
						</div>
						<div class="col-md-2"></div>
					</div>
				</div>
				</form>
				</div>
			</div>
		</div>
	</section>
@endsection