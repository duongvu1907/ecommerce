@extends("admin.layout")
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/lib/chosen/chosen.min.css')}}">
@endsection
@section('content')
@if ($errors->any())
<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show row">
	<span class="fa fa-thumbs-down"></span>
	<div class="alert alert-danger">
		<ul><p>
			@foreach ($errors->all() as $error)
			<?php echo $error ?> | 
			@endforeach
		</p>
	</ul>
</div>
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	<span aria-hidden="true">×</span>
</button>
</div>
<script type="text/javascript">
	jQuery(".alert").alert();
</script>
@endif
<div class="card">
	<div class="card-header">
		<h4>Discounts for Customers</h4>
	</div>
	<div class="card-body">
		<form action="{{url('/admin/'.$slug.'/discount/mixed-customer')}}" method="post">
			@csrf
			<div class="row">
			<div class="col-md-4">
				<select name="discount_id"  required="" class="form-control">
					<option value="" > Choose a discount</option>
					<?php foreach ($discounts as $discount): ?>
						<option value="{{$discount->id}}">{{$discount->name}}</option>
					<?php endforeach ?>
				</select>
			</div>
			<div class="col-md-4">
				<select name="customer_id[]" data-placeholder="Choose customer ..." multiple class="AccSelect" required="" class="form-control">
					<option value="" label="default">Choose a customer</option>
					<?php foreach ($customers as $customer): ?>
						<option value="{{$customer->id}}">{{$customer->email}}</option>
					<?php endforeach ?>
				</select>
			</div>
			<div class="col-md-4">
				<button class="btn btn-danger" type="submit">Mixed</button>
			</div>
		</div>
		</form>
	</div>
</div>
<div class="card">
	<div class="card-header">
		<h4>Discounts for Books</h4>
	</div>
	<div class="card-body">
		<form action="{{url('/admin/'.$slug.'/discount/mixed-book')}}" method="post">
			@csrf
			<div class="row">
			<div class="col-md-4">
				<select name="discount_id" required="" class="form-control">
					<option value=""> Choose a discount</option>
					<?php foreach ($discounts as $discount): ?>
						<option value="{{$discount->id}}">{{$discount->name}}</option>
					<?php endforeach ?>
				</select>
			</div>
			<div class="col-md-4">
				<select name="book_id[]" data-placeholder="Choose books ..." multiple class="AccSelect">
							<option value="" label="default"></option>
							<?php foreach ($books as $book): ?>
								<option value="{{$book->id}}">{{$book->name}}</option>
							<?php endforeach ?>
						</select>
			</div>
			<div class="col-md-4">
				<button class="btn btn-danger" type="submit">Mixed</button>
			</div>
		</div>
		</form>
	</div>
</div>
<div class="card">
	<div class="card-header bg-danger">
		<h4 class="text-white">Discounts for Customers,Books </h4>
		<br>
		<div class="row">
			<div class="col-md-4">
				<select name="discount_id" id="discount_id" class="form-control" >
					<option value="">Choose a discount</option>
					<?php foreach ($discounts as $discount): ?>
						<option value="{{$discount->id}}">{{$discount->name}}</option>
					<?php endforeach ?>
				</select>
			</div>
			<div class="col-md-4">
				<select name="choose" id="choose" class="form-control" >
					<option value="">Choose filter</option>
					<option value="customer">Customer</option>
					<option value="book">Book</option>
				</select>
			</div>
			<div class="col-md-4">
				<button class="btn btn-primary" id="statistics">Statistics</button>
			</div>
		</div>
	</div>
	<div class="card-body">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Customer Or Book</th>
					<th>Discount</th>
					
					<th>Status</th>
					<th></th>
				</tr>
			</thead>
			<tbody class="tbody_data">
				
			</tbody>
		</table>
	</div>
</div>
<script src="{{ asset('admin/assets/js/lib/chosen/chosen.jquery.min.js')}}"></script>
<script>
	jQuery(document).ready(function() {
		jQuery(".AccSelect").chosen({
			disable_search_threshold: 10,
			no_results_text: "Oops, nothing found!",
			width: "100%"
		});
	});
	function bitOr(i){
		if (i==1) {
			return "<span class='text-success'>Available</span>"
		}
		return  "<span class='text-danger'>Expiried</span>"
	}
	jQuery("#statistics").click(function(event) {
		var discount_id = jQuery("#discount_id").val();
		var href = "<?php echo url('admin/'.$slug.'/discount/mixed/') ?>";
		var choose = jQuery("#choose").val();
		console.log(discount_id);
		if (typeof(discount_id)== "undefined" || typeof(choose)=="undefined") {
			alert("Choose ....... :)"); 
		}else{
			jQuery.ajax({
				url: "<?php echo url('admin/'.$slug.'/discount/statistics') ?>",
				type: 'post',
				dataType: 'json',
				data: {'discount_id': discount_id,'choose':choose,'_token':"<?php echo csrf_token() ?>"},
				success:function(data){
					jQuery(".tbody_data").find('tr').remove();
					jQuery.each(data,function(index, el) {
					console.log(el.status);
						var tr = "<tr>"+"<td>"+data[index].name+"</td>"+"<td>"+data[index].discount+"</td>"+"<td>"+bitOr(data[index].status)+"</td>"+"<td><a href='"+href+"/delete/"+data[index].mixed+"/"+data[index].id+"/"+data[index].discount_id+"' onclick='return confirm()'><span class='fa fa-trash text-danger'></span></a></td>"+"</tr>";
						jQuery(".tbody_data").append(tr);
					});
				}
			})
			
		}

	});
</script>
@endsection