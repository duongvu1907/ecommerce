@extends("admin.layout")
@section('css')
	<link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/lib/chosen/chosen.min.css')}}">
	<style type="text/css">
		.abort{
			    position: absolute;
			    top: 72px;
			    left: 16px;
			    width: 90%;
			    padding: 5px 20px;
			    background: #fff;
			    z-index: 555;
		}
		.abort li{
			text-decoration: none;
			list-style: none;
			border-bottom: 1px solid rgba(0,0,0,.5);
			padding: 3px 10px;
			cursor: pointer;
			
		}
		.abort li:hover{
			color:#fff;
			background: lavender;
		}
	</style>
@endsection
@section('content')
	<div class="card">
		<div class="card-header">
			<h4>Add Quantity</h4>
		</div>
		<div class="card-body">
			<form action="{{url('admin/'.$slug.'/book-branch/add')}}" method="post">
				@csrf
				<div class="row">
					<div class="col-md-6" style="position: relative;">
						<label>Name Book</label>
						<input type="number" name="book_id" id="id" required="" style="display: none">
						<input type="text" id="book-branch" class="form-control" required="" autocomplete="off">
						<div class="abort" >
							<ul class="data-bort">
								
							</ul>
						</div>
					</div>
					<div class="col-md-6">
						<label>Branch</label>
						<select name="branch_id" class="form-control" required>
							@if(count($branches)>1)
							<option value="">Choose a branch </option>
							<?php foreach ($branches as $branch): ?>
								<option value="{{$branch->id}}">{{$branch->name}}</option>
							<?php endforeach ?>
							@else
								<option value="{{$branches[0]->id}}" selected>{{$branches[0]->name}}</option>
							@endif
						</select>
					</div>
				</div>
				<div class="row" style="margin-top: 20px;">
					<div class="col-md-6">
						
					</div>
					<div class="col-md-3">
						<label>Quantity</label>
						<input type="number" name="quantity" required="" min="1" class="form-control">
					</div>
					<div class="col-md-3 text-center">
						<button class="btn btn-success" type="submit">Add</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="card">
		<div class="card-header">
			<h4>Book Available</h4>
		</div>
		<div class="card-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Branch</th>
						<th>Quanity</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($books_branches as $bb): ?>
						<tr>
							<td>{{$bb->id}}</td>
							<td>{{$bb->book->name}}</td>
							<td>{{$bb->branch->name}}</td>
							<td>{{$bb->quantity}}</td>
							<td></td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
			<div class="div">
				{{$books_branches->links()}}
			</div>
		</div>
	</div>
@endsection
@section('script')
	<script type="text/javascript">
		
		jQuery("#book-branch").keypress(function(event) {
			jQuery(".abort").css('display', 'block');
			var name  = jQuery(this).val();
			var _token = "<?php echo csrf_token() ?>";
			jQuery.ajax({
				url: "<?php echo url('admin/'.$slug.'/book-branch/ajax') ?>",
				type: 'POST',
				dataType: 'json',
				data: {'name':name,'_token':_token},
				success:function(data){
					jQuery(".abort").find('li').remove();
					jQuery.each(data,function(index, el) {
						jQuery(".data-bort").append("<li onclick='getData(this)' data-book='"+el.id+"''>"+el.name+"</li>")
					});
				}
			})
			
		});
		function getData(li){
			jQuery("#book-branch").val(jQuery(li).text());
			jQuery("#id").val(jQuery(li).attr('data-book'));
			jQuery(".abort").css('display', 'none');
		}
	</script>
@endsection