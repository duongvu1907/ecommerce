@extends("admin.layout")
@section('css')
	<link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/lib/chosen/chosen.min.css')}}">
@endsection
@section('content')
<div class="row">
	<div class="col-md-3"></div>
	<div class="col-md-6">
		<div class="card">
			<div class="card-header">
				<h4>Customer</h4>
			</div>
			<div class="card-body">
			<ul class="text-center">
			<li style="list-style: none;">{{$customer->name}}</li>
			<li style="list-style: none;">{{$customer->email}}</li>
			<li style="list-style: none;">{{$customer->phone}}</li>
			<li style="list-style: none;">{{$customer->address}}</li>
			<li style="list-style: none;">{{$customer->branch->name}}</li>
		</ul>
		
			</div>
		</div>
	</div>
	<div class="col-md-3"></div>
</div>
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10">
			<div class="card">
				<div class="card-header">
					<h4>Active Order</h4>
					<br>
					@if($order->status!=1)
					<form action="{{url('/admin/'.$slug.'/orders/success/'.$order->id)}}" method="get">
						<button class="btn btn-success">COD</button>
					</form>
					@else
						<span class="text-danger">Delivered</span>
					@endif
				</div>
				<div class="card-body">
					<table class="table">
						<tr>
							<th>#</th>
							<th>Book</th>
							<th>Branch</th>
							<th>Quantity</th>
							
						</tr>
						<?php foreach ($orders_details as $bb): ?>
							<tr>
								<td>{{$bb->id}}</td>
								<td>{{$bb->book->name}}</td>
								<td>{{$bb->branch->name}}</td>
								<td>{{$bb->quantity}}</td>
							</tr>
						<?php endforeach ?>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-1"></div>
	</div>
@endsection