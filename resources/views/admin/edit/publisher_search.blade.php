@extends("admin.layout")
@section('css')
	<link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/lib/chosen/chosen.min.css')}}">
@endsection
@section('content')
	<form id="search-publisher">
		<div class="form-group">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<div class="card">
                            <div class="card-header">
                                <strong class="card-title">Search publisher By Name</strong>
                            </div>
                            <div class="card-body">

                              <select data-placeholder="Choose a publisher..." class="publisherSelect" tabindex="1" required="">
                                <option value="" label="default"></option>
                                <?php foreach ($publishers as $publisher): ?>
                                	<option value="{{$publisher->id}}">{{$publisher->name}}</option>
                                <?php endforeach ?>

                            </select>

                        </div>
                    </div>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
	</form>
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10">
			<div class="card">
				<div class="card-header bg-success">
					<h4>List Books  Of <strong id="publisher-index"></strong></h4>
				</div>
				<div class="card-body">
					<table class="table table-hover">
						<thead>
							<th>#</th>
							<th>Name</th>
							<th>Availble</th>
							<th>Action</th>
						</thead>
						<tbody class="listOfpublisher">
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-1"></div>
	</div>
	<script src="{{ asset('admin/assets/js/lib/chosen/chosen.jquery.min.js')}}"></script>
	<script>
    jQuery(document).ready(function() {
        jQuery(".publisherSelect").chosen({
            disable_search_threshold: 10,
            no_results_text: "Oops, nothing found!",
            width: "100%"
        });
    });
    jQuery(".publisherSelect").change(function(event) {
    	var publisher_id = jQuery(this).val();

    	var token = "<?php echo csrf_token() ?>";
    	jQuery.ajax({
    		url: "<?php echo url('admin/'.$slug.'/publisher/ajax') ?>",
    		type: 'POST',
    		dataType: 'json',
    		data: {'publisher_id': publisher_id,'_token':token},
    		success:function(data){
          jQuery(".listOfpublisher").find('tr').remove();
          console.log(data);
          jQuery.each(data,function(index, el) {
             var href = "<?php echo url('admin/'.$slug.'/book') ?>";
             var tr = "<tr>"+"<td>"+data[index].id+"</td>"+"<td>"+data[index].name+"</td>"+"<td>"+data[index].available+"</td>"+"<td><a href='"+href+"?id="+data[index].id+"'><span class='fa fa-edit text-success'></span></a>&nbsp;&nbsp;<a href='"+href+"/delete/"+data[index].id+"' onclick='return confirm()'><span class='fa fa-trash text-danger'></span></a></td>"+"</tr>";
             console.log(tr);
             jQuery(".listOfpublisher").append(tr);
          });
    		},
    		error:function(){
    			console.log('error');
    		}
    	})
    .done(function() {
   	console.log("success");
   })
   .fail(function() {
   	console.log("error");
   })
   .always(function() {
   	console.log("complete");
   });
   
    });

</script>
@endsection