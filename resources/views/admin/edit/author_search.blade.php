@extends("admin.layout")
@section('css')
	<link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/lib/chosen/chosen.min.css')}}">
@endsection
@section('content')
	<form id="search-author">
		<div class="form-group">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<div class="card">
                            <div class="card-header">
                                <strong class="card-title">Search Author By Name</strong>
                            </div>
                            <div class="card-body">

                              <select data-placeholder="Choose a author..." class="AuthorSelect" tabindex="1" required="">
                                <option value="" label="default"></option>
                                <?php foreach ($authors as $author): ?>
                                	<option value="{{$author->id}}">{{$author->name}}</option>
                                <?php endforeach ?>

                            </select>

                        </div>
                    </div>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
	</form>
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10">
			<div class="card">
				<div class="card-header">
					<h4>List Books  Of <strong id="author-index"></strong></h4>
				</div>
				<div class="card-body">
					<table class="table table-hover" id="booksofauthor">
						<thead>
							<th>#</th>
							<th>Name</th>
							<th>Availble</th>
							<th>Action</th>
						</thead>
						<tbody class="listOfauthor">
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-1"></div>
	</div>
	<script src="{{ asset('admin/assets/js/lib/chosen/chosen.jquery.min.js')}}"></script>
	<script>
    jQuery(document).ready(function() {
        jQuery(".AuthorSelect").chosen({
            disable_search_threshold: 10,
            no_results_text: "Oops, nothing found!",
            width: "100%"
        });
    });
    jQuery(".AuthorSelect").change(function(event) {
    	var author_id = jQuery(this).val();

    	var token = "<?php echo csrf_token() ?>";
    	jQuery.ajax({
    		url: "<?php echo url('admin/'.$slug.'/author/ajax') ?>",
    		type: 'POST',
    		dataType: 'json',
    		data: {'author_id': author_id,'_token':token},
    		success:function(data){
          jQuery(".listOfauthor").find('tr').remove();
    			jQuery.each(data,function(index, el) {
             // var data = JSON.parse(el);
            console.log(data);
             var href = "<?php echo url('admin/'.$slug.'/book') ?>";
             var tr = "<tr>"+"<td>"+data[index].id+"</td>"+"<td>"+data[index].name+"</td>"+"<td>"+data[index].available+"</td>"+"<td><a href='"+href+"?id="+data[index].id+"'><span class='fa fa-edit text-success'></span></a>&nbsp;&nbsp;<a href='"+href+"/delete/"+data[index].id+"' onclick='return confirm()'><span class='fa fa-trash text-danger'></span></a></td>"+"</tr>";
             
             jQuery(".listOfauthor").append(tr);
          });
    		},
    		error:function(){
    			console.log('error');
    		}
    	})
    
    });

</script>
@endsection