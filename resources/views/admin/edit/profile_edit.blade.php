@extends('admin.layout')
@section('content')
<div class="row">
	<div class="col-md-1"></div>
	<div class="col-md-10">
		<div class="card">
			<div class="card-header">
				<h4>Edit User</h4>
			</div>
		
		<div class="card-body">
			<form action="{{url('/admin/'.$slug.'/profile/save')}}" method="post">
				@csrf
			<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Name</label>
					</div>
					<div class="col-md-4">
						<input type="text" name="name" value="{{$user->name}}" class="form-control">
					</div>
					<div class="col-md-4"></div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Email</label>
					</div>
					<div class="col-md-4">
						<input type="text" name="email" disabled="" value="{{$user->email}}" class="form-control">
					</div>
					<div class="col-md-4"></div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Branch</label>
					</div>
					<div class="col-md-4">
						<p>{{$user->branch->name}}</p>
					</div>
					<div class="col-md-4"></div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Password</label>
					</div>
					<div class="col-md-4">
						<input type="password" name="password" class="form-control" >
					</div>
					<div class="col-md-4"></div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
											</div>
					<div class="col-md-4">
						<button class="btn  btn-success" type="submit">Edit</button>
					</div>
					<div class="col-md-4"></div>
				</div>
			</div>
		</form>
		</div>
		</div>
	</div>
	<div class="col-md-1"></div>
</div>
@endsection