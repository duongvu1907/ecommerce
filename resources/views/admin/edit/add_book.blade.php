@extends("admin.layout")
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/lib/chosen/chosen.min.css')}}">
@endsection
@section('content')
@if ($errors->any())
		<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show row">
			<span class="fa fa-thumbs-down"></span>
			<div class="alert alert-danger">
				<ul><p>
					@foreach ($errors->all() as $error)
					<?php echo $error ?> | 
					@endforeach
					</p>
				</ul>
			</div>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
		</div>
			<script type="text/javascript">
				jQuery(".alert").alert();
			</script>
			@endif
<div class="card">
	<div class="card-header">
		<h4>Create Book</h4>
	</div>
	<div class="card-body">

		<form action="{{ url('/admin/'.$slug.'/book/add') }}" method="post" enctype="multipart/form-data">
			@csrf
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label for="name">Title</label>
					</div>
					<div class="col-md-8">
						<input type="text" name="name" id="name" class="form-control">
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label for="public">Status</label>
					</div>
					<div class="col-md-8">
						<select name="public" id="public" class="form-control">
							<option value="1">Public</option>
							<option value="0" selected >Non Public</option>
						</select>
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label for="price">Price</label>
					</div>
					<div class="col-md-8">
						<input type="number" name="price" id="price" class="form-control" >
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label for="edition">Edition</label>
					</div>
					<div class="col-md-8">
						<input type="number" name="edition" id="edition" class="form-control" value="1">
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label for="author">Author</label>
					</div>
					<div class="col-md-8">
						<select name="author_id" data-placeholder="Choose a Author..." class="AccSelect" tabindex="1">
							<option value="" label="default"></option>
							<?php foreach ($authors as $author): ?>
								<option value="{{$author->id}}">{{$author->name}}</option>
							<?php endforeach ?>
						</select>
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label for="genres">Genres</label>
					</div>
					<div class="col-md-8">
						<select name="genre[]" data-placeholder="Choose Genres ..." multiple class="AccSelect">
							<option value="" label="default"></option>
							<?php foreach ($genres as $genre): ?>
								<option value="{{$genre->id}}">{{$genre->name}}</option>
							<?php endforeach ?>
						</select>
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label for="publisher">Publisher</label>
					</div>
					<div class="col-md-8">
						<select name="publisher_id" data-placeholder="Choose Publisher ..." tabindex="1" class="AccSelect">
							<option value="" label="default"></option>
							<?php foreach ($publishers as $publisher): ?>
								<option value="{{$publisher->id}}">{{$publisher->name}}</option>
							<?php endforeach ?>
						</select>
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label for="cover images">Cover Image</label>
					</div>
					<div class="col-md-8">
						<input type="file" required="" name="image">
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						
					</div>
					<div class="col-md-8">
						<button class="btn btn-success">Create</button>
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
		</form>
	</div>
</div>
<script src="{{ asset('admin/assets/js/lib/chosen/chosen.jquery.min.js')}}"></script>
<script>
	jQuery(document).ready(function() {
		jQuery(".AccSelect").chosen({
			disable_search_threshold: 10,
			no_results_text: "Oops, nothing found!",
			width: "100%"
		});
	});
</script>
@endsection