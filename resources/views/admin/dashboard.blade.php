@extends("admin.layout")

@section('content')
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
		<div class="card">
			<div class="card-header">
				<h4>Order</h4>			
			</div>
			<div class="card-body">
			
			<table class="table">
				<tr>
					
					<th>Order Id</th>
					<th>Customer id</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
				<?php foreach ($orders as $order): ?>
					<tr>
						<td>{{$order->id}}</td>
						<td>{{$order->customer->id}}</td>
						<td>
							@if($order->status==0)
							<span class="fa fa-times text-danger"></span>
							@else
							<span class="fa fa-check text-success"></span>
							@endif
						</td>
						<td>
							@if($order->status==0)
							<a href="{{url('/admin/'.$slug.'/orders/active/'.$order->id)}}"> <span class="text-success">Processing</span> </a>
							@else
							<a href="{{url('/admin/'.$slug.'/orders/active/'.$order->id)}}"> <span class="text-success">View</span> </a>&nbsp;&nbsp;
							<a href="{{url('/admin/'.$slug.'/orders/delete/'.$order->id)}}" onclick="return confirm('are you sure ?')"><span class="text-danger">Delete</span></a>
							@endif
						</td>
					</tr>
				<?php endforeach ?>
			</table>
		</div>
			</div>
		</div>
		<div class="col-md-2"></div>
	</div>
	<div class="row">
		<div class="col-md-5">
			<div class="card">
				<div class="card-header">
					<h4>Top 10 Books Sellest</h4>
				</div>
				<div class="card-body">
					<table class="table">
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Price</th>
							<th>Quantity</th>
							<th></th>
						</tr>
						<?php foreach ($books as $book): ?>
							<tr>
								<td>{{$book->id}}</td>
								<td>{{$book->name}}</td>
								<td>{{$book->price}}</td>
								<td>{{$book->quantity}}</td>
							</tr>
						<?php endforeach ?>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-7">
			<div class="card">
				<div class="card-header">
					<h4>Top 10 Bill</h4>
				</div>
				<div class="card-body">
					<table class="table">
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Email</th>
							<th>Address</th>
							<th>Phone</th>
							<th>Quantity</th>
						</tr>

						<?php foreach ($customers as $customer): ?>
							<tr>
								<td>{{$customer->id}}</td>
								<td>{{$customer->name}}</td>
								<td>{{$customer->email}}</td>
								<td>{{$customer->address}}</td>
								<td>{{$customer->phone}}</td>
								<td>{{$customer->quantity}}</td>
							</tr>
						<?php endforeach ?>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">
					<h4>Top 10 Books Rate</h4>
				</div>
				<div class="card-body">
					
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">
					<h4>Branhes Ranking</h4>
				</div>
				<div class="card-body">
					<table class="table">
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Location</th>
							<th>SLUG</th>
							
						</tr>
						@if($branches)

						<?php foreach ($branches as $branch): ?>
							<tr>
								<td>{{isset($branch->id)?$branch->id:""}}</td>
								<td>{{isset($branch->name)?$branch->name:""}}</td>
								<td>{{isset($branch->address)?$branch->address:""}}</td>
								<td>{{isset($branch->slug)?$branch->slug:""}}</td>
								<td>{{isset($branch->quantity)?$branch->quantity:""}}</td>
							</tr>
						<?php endforeach ?>
						@endif
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection