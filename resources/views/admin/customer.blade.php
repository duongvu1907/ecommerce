@extends('admin.layout')
@section('content')
<div class="card">
	<div class="card-header">
		<h4>List Of Customers</h4>
	</div>
	<div class="card-body">
		<table class="table table-hover">
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Username</th>
				<th>Email</th>
				<th>Address</th>
				<th></th>
			</tr>
			<?php foreach ($customer as $c): ?>
				<tr>
					<td>{{$c->id}}</td>
					<td>{{$c->name}}</td>
					<td>{{$c->username}}</td>
					<td>{{$c->email}}</td>
					<td>{{$c->address}}</td>
					<td>
						<a href="{{url('/admin/'.$slug.'/customer/orders/'.$c->id)}}">
							<span class="text-success">Bill</span>
						</a>
					</td>
				</tr>
			<?php endforeach ?>
		</table>
	</div>
</div>
@endsection