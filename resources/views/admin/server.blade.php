@extends("admin.layout")
@section('content')
<div class="card">
	<div class="card-header">
		<h4>Server : {{$server['host']}}</h4>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<form action="{{url('admin/'.$slug.'/server/save')}}" method="post">
					@csrf
				<ul style="list-style: none;text-align: center">
					<li>Host : {{$server['host']}}</li>
					<li>DB : {{$server['database']}}</li>
					<li>Port : {{$server['port']}}</li>
					<li>Location : {{$server['location']}}</li>
					<?php $status = config('status.'.Auth::user()->connection); ?>
					<li>Status Config
					<select name="config" id="config">
						<option value="maintenance">maintenance</option>
						<option value="running">Running</option>
					</select>
					<script type="text/javascript">
						var status = "<?php echo $status ?>";
						var options = jQuery("#config").find('option');
						if (options.eq(0).val() == status) {
							options.eq(0).attr('selected', 'true');
						}
						if (options.eq(1).val() == status) {
							options.eq(1).attr('selected', 'true');
						}
					</script>
					</li>
				</ul>
				<br>
				<div class="text-center">
					<button class="btn btn-outline-success" type="submit">Save</button>
				</div>
				</form>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
</div>
@endsection