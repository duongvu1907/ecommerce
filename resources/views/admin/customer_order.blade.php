@extends('admin.layout')
@section('content')
<?php foreach ($orders as $order): ?>
	
<div class="row">
	<div class="col-md-2"></div>
	<div class="col-md-8">
		<div class="card">
			<div class="card-header">
				<h4>Order_Id : {{$order->id}}</h4>
			</div>
			<div class="card-body">
				<table class="table table-hover">
					<tr>
						<th>#</th>
						<th>Book</th>
						<th>Quantity</th>
						<th>Branch</th>
					</tr>
					<?php foreach ($order->orders_details as $c): ?>
						<tr>
							<td>{{$c->id}}</td>
							<td>{{$c->book->name}}</td>
							<td>{{$c->quantity}}</td>
							<td>{{$c->branch->name}}</td>
						</tr>
					<?php endforeach ?>
				</table>
			</div>
</div>
	</div>
	<div class="col-md-2"></div>
</div>
<?php endforeach ?>
@endsection