@extends("admin.layout")

@section('content')
	<div class="animated fadeIn">
		@if ($errors->any())
		<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show row">
			<span class="fa fa-thumbs-down"></span>
			<div class="alert alert-danger">
				<ul><p>
					@foreach ($errors->all() as $error)
					<?php echo $error ?> | 
					@endforeach
					</p>
				</ul>
			</div>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
		</div>
			<script type="text/javascript">
				jQuery(".alert").alert();
			</script>
			@endif
	<div class="row">

		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<button class="btn btn-outline-primary" id="open_create">Create</button>&nbsp;
					<strong class="card-title">Data Genres</strong>

				</div>
				<div class="card-body">
					<table id="bootstrap-data-table" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>#</th>
								<th>Name</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($genres as $genre): ?>
								<tr>
									<td class="tbl_id">{{$genre->id}}</td>
									<td class="tbl_name">{{ $genre->name }}</td>
									<td>
										<span class="fa fa-edit text-success edit-prds"></span>&nbsp;&nbsp;
										<a href="{{url('admin/'.$slug.'/genre/delete/'.$genre->id)}}" onclick="return confirm('Are you sure ?')"><span class=" fa fa-trash-o text-danger"></span></a>
									</td>
								</tr>
							<?php endforeach ?>
							
						</tbody>
					</table>
					<div style="float: right;">
						{{$genres->links()}}
					</div>
				</div>
			</div>
		</div>
		
		<div class="modal fade" id="Creategenre" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="mediumModalLabel">Create genre</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{url('/admin/'.$slug.'/genre/add')}}" method="post">
                            	@csrf
                            	<div class="form-group">
                            		<div class="row">
                            			<div class="col-md-2"></div>
                            			<div class="col-md-8">
                            				
                            				<label for="name">Name</label>
                            				<input type="text" name="name" id="name"  class="form-control">
           
                            				<button class="btn btn-success" type="submit">Create</button>
                            			</div>
                            			<div class="col-md-2"></div>
                            		</div>
                            	</div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- Edit -->

			<div class="modal fade" id="Editgenre" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="mediumModalLabel">Edit genre</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{url('/admin/'.$slug.'/genre/edit')}}" method="post">
                            	@csrf
                            	<div class="form-group">
                            		<div class="row">
                            			<div class="col-md-2"></div>
                            			<div class="col-md-8">
                            				<input type="text" hidden="" name="id" id="id">
                            				<label for="name">Name</label>
                            				<input type="text" name="name" id="name"  class="form-control">
                       
                            				<button class="btn btn-success" type="submit">Edit</button>
                            			</div>
                            			<div class="col-md-2"></div>
                            		</div>
                            	</div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            
                        </div>
                    </div>
                </div>
            </div>

	</div>
</div>
<script>
		jQuery("#open_create").click(function(event){
			jQuery("#Creategenre").modal("show");
		});
		jQuery(".edit-prds").click(function(){
			var tr = jQuery(this).parent().parent()
			var id = tr.find(".tbl_id").text();
			var name = tr.find(".tbl_name").text();
			
			jQuery("#Editgenre").find("input#id").val(id);
			jQuery("#Editgenre").find("input#name").val(name);
			jQuery("#Editgenre").modal("show");
		})
	</script>
@endsection

@section("script")

@endsection