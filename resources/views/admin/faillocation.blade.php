@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-md-4"></div>
		<div class="col-md-4">
			
			<div class="card">
				<div class="card-header bg-success">
					<h4 class="text-md-center text-center text-uppercase">Lỗi xác thực chi nhánh </h4>
				</div>
				<div class="card-body">
					<p style="padding: 40px;font-size: 20px">
						Tài khoản {{$email}} không thuộc chi nhánh {{$branch->name}} tại địa chỉ {{ $branch->address}} .
					</p>
					<br>
					<p>Vui lòng đăng nhập lại <span style="font-style: 66px">:)</span> &nbsp;&nbsp;( <span id="count">10</span>)</p>
				</div>
			</div>

		</div>
		<!-- <div class="col-md-4"></div> -->
	</div>
	<script>
		var i=10;
		setInterval(function(){
				i--;
				document.getElementById("count").innerHTML = i;
				if (i<=0) {
					window.location.replace("<?php echo url('/login') ?>");
				}
		},1000)

	</script>
@endsection