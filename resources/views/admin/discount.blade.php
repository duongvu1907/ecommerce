@extends("admin.layout")
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/lib/chosen/chosen.min.css')}}">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css">
@endsection
@section('content')
	<div class="animated fadeIn">
		@if ($errors->any())
		<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show row">
			<span class="fa fa-thumbs-down"></span>
			<div class="alert alert-danger">
				<ul><p>
					@foreach ($errors->all() as $error)
					<?php echo $error ?> | 
					@endforeach
					</p>
				</ul>
			</div>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
		</div>
			<script type="text/javascript">
				jQuery(".alert").alert();
			</script>
			@endif
	<div class="row">

		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<button class="btn btn-outline-primary" id="open_create">Create</button>&nbsp;
					<strong class="card-title">Data discounts</strong>

				</div>
				<div class="card-body">
					<div class="container mt-3">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs">
							<li class="nav-item">
								<a class="nav-link active" data-toggle="tab" href="#Available">Available</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#Expiried">Expiried</a>
							</li>
						</ul>

						<!-- Tab panes -->
						<div class="tab-content">
							<div id="Available" class="container tab-pane active"><br>
								<table id="bootstrap-data-table" class="table table-striped table-bordered">
									<thead>
										<tr>
											<th>#</th>
											<th>Name</th>
											<th>Value</th>
											<th>Expiry</th>
											<th>Created_at</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($discounts_available as $discount): ?>
											<tr>
												<td class="tbl_id">{{$discount->id}}</td>
												<td class="tbl_name">{{ $discount->name }}</td>
												<td class="tbl_value">{{ $discount->value }}%</td>
												<td class="tbl_expiry_at">{{ $discount->expiry_at }}</td>
												<td class="tbl_created_at">{{ $discount->created_at }}</td>
												<td>
													<span class="fa fa-edit text-success edit-prds"></span>&nbsp;&nbsp;
													<a href="{{url('admin/'.$slug.'/discount/delete/'.$discount->id)}}" onclick="return confirm('Are you sure ?')"><span class=" fa fa-trash-o text-danger"></span></a>
												</td>
											</tr>
										<?php endforeach ?>

									</tbody>
								</table>
								<div style="float: right;">
									{{$discounts_available->links()}}
								</div>
							</div>
							<div id="Expiried" class="container tab-pane fade"><br>
								<table id="bootstrap-data-table" class="table table-striped table-bordered">
									<thead>
										<tr>
											<th>#</th>
											<th>Name</th>
											<th>Value</th>
											<th>Expiry</th>
											<th>Created_at</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($discounts_expiried as $discount): ?>
											<tr>
												<td class="tbl_id">{{$discount->id}}</td>
												<td class="tbl_name">{{ $discount->name }}</td>
												<td class="tbl_value">{{ $discount->value }}%</td>
												<td class="tbl_expiry_at">{{ $discount->expiry_at }}</td>
												<td class="tbl_created_at">{{ $discount->created_at }}</td>
												<td>
													<span class="fa fa-edit text-success edit-prds"></span>&nbsp;&nbsp;
													<a href="{{url('admin/'.$slug.'/discount/delete/'.$discount->id)}}" onclick="return confirm('Are you sure ?')"><span class=" fa fa-trash-o text-danger"></span></a>
												</td>
											</tr>
										<?php endforeach ?>

									</tbody>
								</table>
								<div style="float: right;">
									{{$discounts_expiried->links()}}
								</div>
							</div>
							
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
		<div class="modal fade" id="Creatediscount" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="mediumModalLabel">Create discount</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{url('/admin/'.$slug.'/discount/add')}}" method="post">
                            	@csrf
                            	<div class="form-group">
                            		<div class="row">
                            			<div class="col-md-2"></div>
                            			<div class="col-md-8">
                            				
                            				<label for="name">Name</label>
                            				<input type="text" name="name" id="name"  class="form-control">
                            				<label for="value">Value</label>
                            				<input type="number" name="value" id="value" class="form-control" max="100" min="1" >
                            				<label for="expiry">Expiry</label>
                            				<input type="text" name="expiry_at" id="expiry" class="form-control datepicker" width="40%">
                            				<input type="time" name="time_expiry_at" class="form-control" >
                            				<label for="created_at">Created_at</label>
                            				<input type="text" name="created_at" id="created_at" class="form-control datepicker" >
                            				<input type="time" name="time_created_at" class="form-control" >
                            				<br>
                            				
                            				<button class="btn btn-success" type="submit">Create</button>
                            			</div>
                            			<div class="col-md-2"></div>
                            		</div>
                            	</div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- Edit -->

			<div class="modal fade" id="EditDiscount" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="mediumModalLabel">Edit Discount</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{url('/admin/'.$slug.'/discount/edit')}}" method="post">
                            	@csrf
                            	<div class="form-group">
                            		<div class="row">
                            			<div class="col-md-2"></div>
                            			<div class="col-md-8">
                            				<input type="text" hidden="" name="id" id="id">
                            				<label for="name">Name</label>
                            				<input type="text" name="name" id="name"  class="form-control">
                            				<label for="address">Value</label>
                            				<input type="text" name="value" id="value" class="form-control" >

                            				<label >Expiry</label>
                            				<input type="text" name="expiry_at" id="expiry" class="form-control datepicker" onchange="checkTime(this)" >
											<input type="time" name="time_expiry_at" class="form-control" >
											<label>Start</label>
                            				<input type="text" name="created_at" id="created" class="form-control datepicker" onchange="checkTime(this)">
                            				<input type="time" name="time_created_at" class="form-control" >
                            				<br>
                            				
                            				<button class="btn btn-success checkSubmit" type="submit">Edit</button>
                            			</div>
                            			<div class="col-md-2"></div>
                            		</div>
                            	</div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            
                        </div>
                    </div>
                </div>
            </div>

	</div>
</div>
@endsection
@section('script')
<script src="{{ asset('admin/assets/js/lib/chosen/chosen.jquery.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script>
	jQuery(".datepicker").datepicker({
		format:'dd-mm-yyyy'
	});
	jQuery(".datepicker2").datepicker({
		format:'dd-mm-yyyy'
	});
	jQuery(".datepicker3").datepicker({
		format:'dd-mm-yyyy'
	});
	
	jQuery("#created_at").datepicker("setDate",'now');



	jQuery(document).ready(function() {
		jQuery(".AccSelect").chosen({
			disable_search_threshold: 10,
			no_results_text: "Oops, nothing found!",
			width: "100%"
		});
	});

	jQuery("#open_create").click(function(event){
			jQuery("#Creatediscount").modal("show");
		});
		jQuery(".edit-prds").click(function(){
			var tr = jQuery(this).parent().parent();
			var id = tr.find(".tbl_id").text();
			var name = tr.find(".tbl_name").text();
			var value = tr.find(".tbl_value").text().replace("%","");
			var expiry_at = tr.find(".tbl_expiry_at").text();
			var created_at = tr.find(".tbl_created_at").text();
			jQuery("#EditDiscount").find("input#id").val(id);
			jQuery("#EditDiscount").find("input#name").val(name);
			jQuery("#EditDiscount").find("input#value").val(value);
			
			jQuery("#EditDiscount").modal("show");
		})
		jQuery(".checkSubmit").attr('disable', true);
		function checkTime(btn){
			var tr = jQuery(this).parent();
			var expiry_at = Date.parse(tr.find(".tbl_expiry_at").text());
			var created_at = Date.parse(tr.find(".tbl_created_at").text());
			if (expiry_at>=created_at) {
				jQuery(".checkSubmit").attr('disable', false);
			}
		}
</script>
@endsection
