@extends("admin.layout")

@section('content')
	<div class="animated fadeIn">
		@if ($errors->any())
		<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show row">
			<span class="fa fa-thumbs-down"></span>
			<div class="alert alert-danger">
				<ul><p>
					@foreach ($errors->all() as $error)
					<?php echo $error ?> | 
					@endforeach
					</p>
				</ul>
			</div>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
		</div>
			<script type="text/javascript">
				jQuery(".alert").alert();
			</script>
			@endif
	<div class="row">

		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<button class="btn btn-outline-primary" id="open_create">Create</button>&nbsp;
					<strong class="card-title">Data authors</strong>

				</div>
				<div class="card-body">
					<table id="bootstrap-data-table" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>#</th>
								<th>Name</th>
								<th>Address</th>
								<th>Jobs</th>
								<th>Work</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($authors as $author): ?>
								<tr>
									<td class="tbl_id">{{$author->id}}</td>
									<td class="tbl_name">{{ $author->name }}</td>
									<td class="tbl_address">{{ $author->address }}</td>
									<td class="tbl_job">{{ $author->job }}</td>
									<td class="tbl_count">{{ $author->books()->count() }}</td>
									<td>
										<span class="fa fa-edit text-success edit-prds"></span>&nbsp;&nbsp;
										<a href="{{url('admin/'.$slug.'/author/delete/'.$author->id)}}" onclick="return confirm('Are you sure ?')"><span class=" fa fa-trash-o text-danger"></span></a>
									</td>
								</tr>
							<?php endforeach ?>
							
						</tbody>
					</table>
					<div style="float: right;">
						{{$authors->links()}}
					</div>
				</div>
			</div>
		</div>
		
		<div class="modal fade" id="CreateAuthor" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="mediumModalLabel">Create Author</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{url('/admin/'.$slug.'/author/add')}}" method="post">
                            	@csrf
                            	<div class="form-group">
                            		<div class="row">
                            			<div class="col-md-2"></div>
                            			<div class="col-md-8">
                            				
                            				<label for="name">Name</label>
                            				<input type="text" name="name" id="name"  class="form-control">
                            				<label for="address">Address</label>
                            				<input type="text" name="address" id="address" class="form-control" >
                            				<label for="job">Jobs</label>
                            				<input type="text" name="job" id="job" class="form-control" >
                            				<br>
                            				
                            				<button class="btn btn-success" type="submit">Create</button>
                            			</div>
                            			<div class="col-md-2"></div>
                            		</div>
                            	</div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- Edit -->

			<div class="modal fade" id="EditAuthor" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="mediumModalLabel">Edit Author</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{url('/admin/'.$slug.'/author/edit')}}" method="post">
                            	@csrf
                            	<div class="form-group">
                            		<div class="row">
                            			<div class="col-md-2"></div>
                            			<div class="col-md-8">
                            				<input type="text" hidden="" name="id" id="id">
                            				<label for="name">Name</label>
                            				<input type="text" name="name" id="name"  class="form-control">
                            				<label for="address">Address</label>
                            				<input type="text" name="address" id="address" class="form-control" >
                            				<label for="job">Jobs</label>
                            				<input type="text" name="job" id="job" class="form-control" >
                            				<br>
                            				
                            				<button class="btn btn-success" type="submit">Edit</button>
                            			</div>
                            			<div class="col-md-2"></div>
                            		</div>
                            	</div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            
                        </div>
                    </div>
                </div>
            </div>

	</div>
</div>
<script>
		jQuery("#open_create").click(function(event){
			jQuery("#CreateAuthor").modal("show");
		});
		jQuery(".edit-prds").click(function(){
			var tr = jQuery(this).parent().parent()
			var id = tr.find(".tbl_id").text();
			var name = tr.find(".tbl_name").text();
			var address = tr.find(".tbl_address").text();
			var job = tr.find(".tbl_job").text();
			jQuery("#EditAuthor").find("input#id").val(id);
			jQuery("#EditAuthor").find("input#name").val(name);
			jQuery("#EditAuthor").find("input#address").val(address);
			jQuery("#EditAuthor").find("input#job").val(job);
			jQuery("#EditAuthor").modal("show");
		})
	</script>
@endsection

@section("script")

@endsection