@extends("admin.layout")
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/lib/chosen/chosen.min.css')}}">
@endsection
@section('content')
@if ($errors->any())
		<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show row">
			<span class="fa fa-thumbs-down"></span>
			<div class="alert alert-danger">
				<ul><p>
					@foreach ($errors->all() as $error)
					<?php echo $error ?> | 
					@endforeach
					</p>
				</ul>
			</div>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
		</div>
			<script type="text/javascript">
				jQuery(".alert").alert();
			</script>
			@endif
<div class="animated fadeIn">
	<div class="row">
		<input type="number" hidden="" id="checkdump" value="<?php echo isset($_GET['id'])?$_GET['id']:0; ?>">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">

					<strong class="card-title">Data Books</strong>
				</div>
				<div class="card-body">
					<table id="bootstrap-data-table" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>#</th>
								<th>Cover</th>
								<th>Name</th>
								<th>Author</th>
								<th>Publisher</th>
								<th>Status</th>
								<th>Quantity Sold</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($books as $book): ?>
								<tr>
									<td class="tbl_id" value="{{$book->id}}">{{$book->id}}</td>
									<td><img src="{{asset('upload/books/'.$book->images)}}" alt="" width="130px;"></td>
									<td class="tbl_name" value="{{ $book->name }}" >{{ $book->name }}</td>
									<td class="tbl_author" value="{{$book->author()->first()->id}}" >{{$book->author()->first()->name}}</td>
									<td class="tbl_publisher" value="{{$book->publisher()->first()->id}}" >{{$book->publisher()->first()->name}}</td>
									<td class="tbl_public" value="{{$book->public}}" >
										@if($book->public)
										<span class="fa fa-check text-success"></span>
										@else
										<span class="fa fa-ban text-danger"></span>
										@endif
									</td>
									<td>{{ $book->orders_details()->count() }}</td>
									<td>
										<span class="fa fa-edit text-success edit-prds"></span>&nbsp;&nbsp;
										<a href="{{url('admin/'.$slug.'/book/delete/'.$book->id)}}"><span class="fa fa-trash-o text-danger"></span></a>
									</td>
								</tr>
							<?php endforeach ?>
							
						</tbody>
					</table>
					<div style="float: right;">
						{{ $books->links() }}
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="BookEdit" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="mediumModalLabel">Edit Book</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form action="{{url('admin/'.$slug.'/book/edit')}}" method="post" enctype="multipart/form-data">
							@csrf
							<input type="number" name="id" id="id" hidden="">
							<div class="form-group">
								<div class="row">
									<div class="col-md-2">
										<label for="name">Title</label>
									</div>
									<div class="col-md-8">
										<input type="text" name="name" id="name" class="form-control">
									</div>
									<div class="col-md-2"></div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-2">
										<label for="public">Status</label>
									</div>
									<div class="col-md-8">
										<select name="public" id="public" class="form-control">
											<option value="1">Public</option>
											<option value="0" selected >Non Public</option>
										</select>
									</div>
									<div class="col-md-2"></div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-2">
										<label for="price">Price</label>
									</div>
									<div class="col-md-8">
										<input type="number" name="price" id="price" class="form-control" >
									</div>
									<div class="col-md-2"></div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-2">
										<label for="edition">Edition</label>
									</div>
									<div class="col-md-8">
										<input type="number" name="edition" id="edition" class="form-control" value="1">
									</div>
									<div class="col-md-2"></div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-2">
										<label for="author_id">Author</label>
									</div>
									<div class="col-md-8">
										<select name="author_id" id="author_id" data-placeholder="Choose a Author..." class="AccSelect" tabindex="1">
											<option value="" label="default"></option>
											<?php foreach ($authors as $author): ?>
												<option value="{{$author->id}}">{{$author->name}}</option>
											<?php endforeach ?>
										</select>
									</div>
									<div class="col-md-2"></div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-2">
										<label for="genres">Genres</label>
									</div>
									<div class="col-md-8">
										<select name="genre[]" id="genres" data-placeholder="Choose Genres ..." multiple class="AccSelect">
											<option value="" label="default"></option>
											<?php foreach ($genres as $genre): ?>
												<option value="{{$genre->id}}">{{$genre->name}}</option>
											<?php endforeach ?>
										</select>
									</div>
									<div class="col-md-2"></div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-2">
										<label for="publisher">Publisher</label>
									</div>
									<div class="col-md-8">
										<select name="publisher_id" id="publisher_id" data-placeholder="Choose Publisher ..." tabindex="1" class="AccSelect">
											
											<?php foreach ($publishers as $publisher): ?>
												<option value="{{$publisher->id}}">{{$publisher->name}}</option>
											<?php endforeach ?>
										</select>
									</div>
									<div class="col-md-2"></div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-2">
										<label for="cover images">Cover Image</label>
									</div>
									<div class="col-md-8">
										<input type="file"  name="image" id="image">
										<div id="cover-image">
											
										</div>
									</div>
									<div class="col-md-2"></div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-2">

									</div>
									<div class="col-md-8">
										<button class="btn btn-success">Edit</button>
									</div>
									<div class="col-md-2"></div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{{ asset('admin/assets/js/lib/chosen/chosen.jquery.min.js')}}"></script>
<script>
	jQuery(document).ready(function() {
		jQuery(".AccSelect").chosen({
			disable_search_threshold: 10,
			no_results_text: "Oops, nothing found!",
			width: "100%"
		});
	});
</script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		var checkdump = parseInt(jQuery('#checkdump').val());
		if (checkdump!=0) {
			var _token = "<?php echo csrf_token() ?>";
			var folder_img = "<?php echo asset('upload/books/') ?>";
			ajaxBook(checkdump,_token,folder_img);
		}
	});
	jQuery(".edit-prds").click(function(event) {
		var tr = jQuery(this).parent().parent();
		var id = tr.find('td.tbl_id').attr('value');
		var _token = "<?php echo csrf_token() ?>";
		var folder_img = "<?php echo asset('upload/books/') ?>";
		ajaxBook(id,_token,folder_img);
	});
	function ajaxBook(id,_token,folder_img){
		jQuery.ajax({
			url: "<?php echo url('admin/'.$slug.'/book/ajax_edit/') ?>",
			type: 'POST',
			dataType: 'json',
			data: {'id': id,'_token':_token},
			success:function(data){
				console.log(data);
				var modal = jQuery("#BookEdit");
				modal.find("#id").val(data["book"].id);
				modal.find("#name").val(data["book"].name);
				modal.find('#price').val(data["book"].price);
				modal.find('#publisher_id').val(data["book"].publisher_id).trigger('chosen:updated');
				console.log(data["book"].publisher_id);
				modal.find('#author_id').val(data["book"].author_id).trigger('chosen:updated');
				var arr_genres = new Array();
				for (var i = 0; i < data['genres'].length; i++) {
					arr_genres.push(data['genres'][i].id);
				}
				console.log(arr_genres);
				modal.find('#genres').val(arr_genres).trigger('chosen:updated');
				// var img = "<img src='"+folder_img+"/"+data["book"].images+" >";
				
				if (jQuery("#output-img").length==0) {
					modal.find('#cover-image').append("<img src='' alt='' width='150px' id='output-img'>");
				}
					modal.find('#cover-image').find('img').attr('src', folder_img+"/"+data["book"].images);
				modal.modal('show');
			}
		});
	}
	function readURL(input) {

		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function(e) {
				jQuery('#output-img').attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	}

	jQuery("#image").change(function() {
		readURL(this);
	});
</script>
@endsection
@section("script")

@endsection